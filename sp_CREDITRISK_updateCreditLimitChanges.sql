
USE [Tetra_Audit_Analytics_PRD]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE sp_CREDITRISK_updateCreditLimitChanges
with exec as caller
as
begin

DROP TABLE IF EXISTS #tbl_CREDITRISK_creditLimitChanges_step1
SELECT

	  SUBSTRING(pos.TABKEY,LEN(pos.TABKEY)-3,4) +''+ BUT000.PARTNER 'PrimaryKey',
	  BUT000.MC_NAME1 as Customer,
	  BUT000.PARTNER as CustomerID,
	  POS.tabname as TableName,
	  POS.Fname as FieldName, 
	  POS.TABKEY as TableKey,
	  SUBSTRING(pos.TABKEY,LEN(pos.TABKEY)-3,4) SegmentID,
	  HDR.USERNAME UserName,
	  HDR.UDATE UDate,
	  HDR.UTIME UTime,
	  POS.CHNGIND ChangeID,
	  CAST(POS.value_old as float)  ValueOld,
	  CAST(POS.value_new as float)  ValueNew,
	  CxFA_old.ConvertedVal ValueOldEur,
	  CxFA_new.ConvertedVal ValueNewEur,
	  POS.CUKY_OLD CukyOld,
	  POS.CUKY_NEW CukyNew,
	  POS.CHANGENR ChangeNR

INTO #tbl_CREDITRISK_creditLimitChanges_step1 

FROM CDHDR HDR 
JOIN CDPOS POS ON POS.OBJECTID = HDR.OBJECTID AND hdr.CHANGENR = pos.CHANGENR AND hdr.OBJECTCLAS = pos.OBJECTCLAS
JOIN BUT000 on pos.objectid = BUT000.PARTNER
OUTER APPLY (SELECT Val AS ConvertedVal, FinalCurr, Trans
                FROM dbo.ufnCustomCurrencyBudgetRate(POS.CUKY_OLD, 'EUR', '2018', CAST(POS.value_old as float))) CxFA_old(ConvertedVal,FinalCurr,Trans)
OUTER APPLY (SELECT Val AS ConvertedVal, FinalCurr, Trans
                FROM dbo.ufnCustomCurrencyBudgetRate(POS.CUKY_NEW, 'EUR', '2018', CAST(POS.value_new as float))) CxFA_new(ConvertedVal,FinalCurr,Trans)
WHERE POS.OBJECTCLAS = 'BUPA_UKM' AND POS.TABNAME = 'UKMBP_CMS_SGM' AND POS.FNAME = 'CREDIT_LIMIT'


DROP table if exists tbl_CREDITRISK_creditLimitChanges
select step1.*,
       CASE WHEN ValueNewEur > ValueOldEur THEN 1 ELSE 0 END Increase,
	   ABS(ValueNewEur - ValueOldEur) amountDifference,	
	   CASE WHEN ValueNewEur > ValueOldEur THEN ABS(ValueNewEur - ValueOldEur) / NULLIF(ValueOldEur,0)
	   ELSE ABS(ValueOldEur - ValueNewEur) / NULLIF(ValueNewEur,0) END PercentageDiff
	  
INTO tbl_CREDITRISK_creditLimitChanges
FROM #tbl_CREDITRISK_creditLimitChanges_step1 step1

DROP table if exists tbl_CREDITRISK_creditLimitChanges_lastChange
SELECT 
     clc.Segment +''+ clc.CustomerID as 'PrimaryKey',
	 clc.CustomerID,
	 clc.Customer,
	 clc.Segment,
	 SUM(rec.GrossReceivablesEUR) GrossReceivablesEUR,
	 convert(varchar,max(UDate),23) as LastUpdateDate,
	 DATEDIFF(YEAR,CONVERT(datetime,max(UDate)),GETDATE()) YearsSinceLastChange,
	 DATEDIFF(MONTH,CONVERT(datetime,max(UDate)),GETDATE()) MonthsSinceLastChange
INTO tbl_CREDITRISK_creditLimitChanges_lastChange
	 from tbl_CREDITRISK_creditLimitChanges_old clc
JOIN tbl_CREDITRISK_creditSegmentReceivablesAggregated rec on clc.CustomerID = rec.SiteID and clc.Segment = rec.CreditSegment 
GROUP BY 
     clc.Customer,
	 clc.CustomerID,
	 clc.Segment

DROP table if exists tbl_CREDITRISK_creditLimitChanges_shortTermLimitIncrease
;WITH CTE AS
(
SELECT
	  CRC.CustomerID,
	  CRC.SegmentID,
	  CRC.UDate,
	  CRC.UTime,
	  CRC.ValueOldEur,
	  CRC.ValueNewEur, 
	  ROW_NUMBER() OVER (PARTITION BY CustomerID,SegmentID ORDER BY CustomerID,TableKey,CRC.UDATE,CRC.UTIME) as rn
FROM tbl_CREDITRISK_creditLimitChanges CRC )

SELECT CTE1.SegmentID +''+ cte1.CustomerID as 'PrimaryKey',
       CTE1.CustomerID,
	   CTE1.SegmentID,
	   CTE1.UDate UDate_Previous,
	   CTE1.UTime UTime_Previous,
	   CTE1.ValueOldEur ValueOldEur_Previous,
	   CTE1.ValueNewEur ValueNewEur_Previous,
	   CTE2.UDate,
	   CTE2.UTime,
	   CTE2.ValueOldEur,
	   CTE2.ValueNewEur,
	   CASE WHEN CAST(CTE1.ValueNewEur as float) > CAST(CTE1.ValueOldEur as float) THEN 1 ELSE 0 END Increase,
	   CASE WHEN CTE1.ValueOldEur = CTE2.ValueNewEur THEN 1 ELSE 0 END NewIsOldValue,
	   CAST(CTE1.ValueNewEur as float) - CAST(CTE1.ValueOldEur as float) Amount,
       DATEDIFF(DAY,(CONVERT(datetime,CTE2.UDATE)),CONVERT(datetime,CTE1.UDATE)) AS DaysSinceLastChange,
	   CTE1.rn as CTE1RowNumber,
	   CTE2.rn as CTE2RowNumber
INTO tbl_CREDITRISK_creditLimitChanges_shortTermLimitIncrease
FROM CTE CTE1
LEFT JOIN CTE CTE2 ON CTE1.CustomerID = CTE2.CustomerID AND CTE1.SEGMENTID = CTE2.SEGMENTID AND CTE1.rn = CTE2.rn + 1
ORDER BY 
       CTE1.CustomerID,
	   CTE1.SegmentID,
	   CTE1.UDate

END 

GO
