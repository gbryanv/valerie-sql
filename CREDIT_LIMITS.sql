
DROP TABLE IF EXISTS #tmpCreditLimitSite
SELECT  DISTINCT
	    CMS.PARTNER as SiteID,
		KNA1.NAME1 SiteName,
		KNA1.NAME1+ '  '+KNA1.KUNNR SiteNameID,
		CMS_SGM.CREDIT_SGMNT SiteCreditSgmnt,
	    SEG.CURRENCY CreditSgmntCurrency,
	    ROUND(CxFA.ConvertedVal,2) SiteCreditLimitEUR,
	    CASE WHEN P1.ADDTYPE = '30' AND P1.DATA_TYPE = '01' THEN 1 ELSE 0 END SiteCheckException	
INTO    #tmpCreditLimitSite
FROM 
	    UKMBP_CMS CMS 
		JOIN UKMBP_CMS_SGM CMS_SGM ON CMS.PARTNER = CMS_SGM.PARTNER
		LEFT JOIN BUT000 on CMS.PARTNER = BUT000.PARTNER AND BUT000.TYPE = 3
	    LEFT JOIN BP3100 P1 ON CMS.PARTNER = P1.PARTNER AND P1.CRITER = CMS_SGM.CREDIT_SGMNT AND P1.ADDTYPE = '30' AND P1.DATA_TYPE = '01' 
	    LEFT JOIN UKMCRED_SGM0C SEG ON SEG.CREDIT_SGMNT = CMS_SGM.CREDIT_SGMNT
	    JOIN KNA1 on CMS.PARTNER = KNA1.KUNNR and KNA1.KTOKD NOT IN ('ICC','ADAC','ZABS','DEBI')  --AND KNA1.LOEVM = ''
	    OUTER APPLY (SELECT Val AS ConvertedVal, FinalCurr, Trans
                FROM dbo.ufnCustomCurrencyBudgetRate(SEG.CURRENCY, 'EUR', '2018', CMS_SGM.CREDIT_LIMIT)) CxFA(ConvertedVal,FinalCurr,Trans)
WHERE   BUT000.PARTNER IS NULL 


DROP TABLE IF EXISTS #tmpCreditLimitSiteCustomer
SELECT  DISTINCT ITS.*,
        BUT050.PARTNER1 CustomerID,
		B.MC_NAME1 CustomerName,
		SGM_P1.CREDIT_SGMNT CustomerCreditSgmnt,
		CASE WHEN P1.ADDTYPE = '30' AND P1.DATA_TYPE = '01' THEN 1 ELSE 0 END CustomerCheckException,
		ROUND(CxFA.ConvertedVal,2) CustomerCreditLimitEUR
INTO #tmpCreditLimitSiteCustomer
FROM #tmpCreditLimitSite ITS
JOIN BUT000 ON BUT000.PARTNER = ITS.SiteID
LEFT JOIN BUT050 ON ITS.SiteID = BUT050.PARTNER2 AND BUT050.RELTYP IN ('ZCG002')
LEFT JOIN BP3100 P1 ON BUT050.PARTNER1 = P1.PARTNER AND P1.CRITER = ITS.SiteCreditSgmnt AND P1.ADDTYPE = '30' AND P1.DATA_TYPE = '01' 
LEFT JOIN UKMBP_CMS_SGM SGM_P1 ON BUT050.PARTNER1 = SGM_P1.PARTNER AND SGM_P1.CREDIT_SGMNT = ITS.SiteCreditSgmnt
LEFT JOIN BUT000 B on BUT050.PARTNER1 = B.PARTNER
OUTER APPLY (SELECT Val AS ConvertedVal, FinalCurr, Trans
                FROM dbo.ufnCustomCurrencyBudgetRate(ITS.CreditSgmntCurrency, 'EUR', '2018', SGM_P1.CREDIT_LIMIT)) CxFA(ConvertedVal,FinalCurr,Trans)
			

DROP TABLE IF EXISTS tbl_CREDITRISK_creditLimitsHierarchy
SELECT  DISTINCT 
        CUST.SiteCreditSgmnt+''+SiteID as PK,
        CUST.*,
		BUT050.PARTNER1 CustomerGroupID,
	    B.MC_NAME1 CustomerGroupName,
		SGM_P1.CREDIT_SGMNT CustomerGroupCreditSgmnt,
		CASE WHEN P1.ADDTYPE = '30' AND P1.DATA_TYPE = '01' THEN 1 ELSE 0 END CustomerGroupCheckException,
		ROUND(CxFA.ConvertedVal,2) CustomerGroupCreditLimitEUR,
		CASE WHEN CUST.SiteCheckException = 0 THEN 'Site' 
		     WHEN CUST.SiteCheckException = 1 AND CUST.CustomerCheckException = 0 AND CUST.CustomerID is not null THEN 'Customer'
			 WHEN CUST.SiteCheckException = 1 AND CUST.CustomerCheckException = 1 AND (P1.ADDTYPE is null or P1.ADDTYPE != '30') AND BUT050.PARTNER1 is not null THEN 'Customer Group'
			 ELSE 'No Limit'
		END CreditLimitLevel,
	    CASE WHEN KNA1.AUFSD <> '' THEN 'Yes' ELSE 'No' END CustomerOrderBlock,    
		CASE WHEN KNA1.LOEVM <> '' THEN 'Yes' ELSE 'No' END CustomerMarkedForDeletion
INTO Tetra_Audit_Analytics_PRD.dbo.tbl_CREDITRISK_creditLimitsHierarchy
FROM #tmpCreditLimitSiteCustomer CUST
LEFT JOIN BUT000 ON BUT000.PARTNER = CUST.CustomerID
LEFT JOIN BUT050 ON CUST.CUSTOMERID = BUT050.PARTNER2 AND BUT050.RELTYP IN ('ZCG001')
LEFT JOIN BP3100 P1 ON BUT050.PARTNER1 = P1.PARTNER AND P1.CRITER = CUST.SiteCreditSgmnt AND P1.ADDTYPE = '30' AND P1.DATA_TYPE = '01' 
LEFT JOIN UKMBP_CMS_SGM SGM_P1 ON BUT050.PARTNER1 = SGM_P1.PARTNER AND SGM_P1.CREDIT_SGMNT = CUST.SiteCreditSgmnt
LEFT JOIN BUT000 B on BUT050.PARTNER1 = B.PARTNER
LEFT JOIN KNA1 on CUST.SiteID = KNA1.KUNNR 
OUTER APPLY (SELECT Val AS ConvertedVal, FinalCurr, Trans
                FROM dbo.ufnCustomCurrencyBudgetRate(CUST.CreditSgmntCurrency, 'EUR', '2018', SGM_P1.CREDIT_LIMIT)) CxFA(ConvertedVal,FinalCurr,Trans)
WHERE CUST.SiteCreditSgmnt IN (Select KKBER from T014T Where KKBER like '%1' or KKBER like '%3') 

DROP TABLE IF EXISTS tbl_CREDITRISK_creditLimit
SELECT distinct
	   h.SiteCreditSgmnt+''+SiteID AS PrimaryKey,
       h.SiteID,
	   h.SiteCreditSgmnt as SegmentID,
	   t.KKBTX as SegmentDescription,
	    CASE 
			WHEN h.CreditLimitLevel  = 'Site' THEN SiteCreditLimitEUR
	        WHEN h.CreditLimitLevel  = 'Customer' THEN CustomerCreditLimitEUR
			WHEN h.CreditLimitLevel  = 'Customer Group' THEN CustomerGroupCreditLimitEUR 
			ELSE ''
	  END AS Limit,
	  CASE WHEN h.CreditLimitLevel = 'Customer' THEN h.CustomerID 
		   WHEN h.CreditLimitLevel IN ('Site','No Limit') THEN h.SiteID 
		   ELSE h.CustomerGroupID
	   END LevelID,
	   CASE WHEN h.CreditLimitLevel IN ('Customer','Customer Group') THEN 'Customer Group'
		    WHEN h.CreditLimitLevel IN ('Site') THEN 'Customer' 
		    ELSE h.CreditLimitLevel
	   END LimitLevel,
	    CASE WHEN h.CreditLimitLevel = 'Customer' THEN h.CustomerName 
	       WHEN h.CreditLimitLevel = 'Customer Group' THEN h.CustomerGroupName
	 ELSE h.SiteNameID
	 END LevelName
	  -- h.OrderBlock as CustomerOrderBlock
INTO tbl_CREDITRISK_creditLimit
FROM tbl_CREDITRISK_creditLimitsHierarchy h
JOIN T014T t on t.KKBER = h.SiteCreditSgmnt



DROP TABLE IF EXISTS tbl_CREDITRISK_creditSegment
SELECT distinct
	 t.KKBER as SegmentID,
	 t.KKBTX as Description
INTO tbl_CREDITRISK_creditSegment
from T014T t 
JOIN tbl_CREDITRISK_creditLimitsHierarchy h on t.KKBER = h.SiteCreditSgmnt


DROP TABLE IF EXISTS tbl_CREDITRISK_customer
SELECT distinct 
      CASE WHEN h.CreditLimitLevel = 'Customer' THEN h.CustomerID 
	       WHEN h.CreditLimitLevel = 'Customer Group' THEN h.CustomerGroupID
	  ELSE h.SiteID END LevelID,
	  CASE WHEN h.CreditLimitLevel = 'Customer' THEN h.CustomerName 
	       WHEN h.CreditLimitLevel = 'Customer Group' THEN h.CustomerGroupName
	  ELSE h.SiteName END LevelName,
      CASE WHEN h.CreditLimitLevel = 'Customer' THEN h.CustomerName 
	       WHEN h.CreditLimitLevel = 'Customer Group' THEN h.CustomerGroupName
	 ELSE h.SiteNameID 
	 END LevelNameID
	 --h.SiteCreditSgmnt SegmentID
INTO tbl_CREDITRISK_customer
FROM tbl_CREDITRISK_creditLimitsHierarchy h


DROP TABLE IF EXISTS tbl_CREDITRISK_customer_detail
SELECT distinct
      h.SiteCreditSgmnt+''+SiteID AS PK,
      CASE WHEN h.CreditLimitLevel = 'Customer' THEN h.CustomerID 
	       WHEN h.CreditLimitLevel = 'Customer Group' THEN h.CustomerGroupID
	  ELSE h.SiteID END LevelID,
	 h.SiteID,
	 h.CreditSgmntCurrency SegmentID
INTO tbl_CREDITRISK_customer_detail
FROM tbl_CREDITRISK_creditLimitsHierarchy h


select top 3* from tbl_CREDITRISK_creditLimit
select top 3* from tbl_CREDITRISK_creditLimitLevel
select top 3* from tbl_CREDITRISK_customer

/*
select distinct(Limit),LimitLevel from tbl_CREDITRISK_creditLimit cl
join tbl_CREDITRISK_customer c on cl.LevelID = c.LevelID
where cl.LevelID = '5000003498' and cl.SegmentID = 'GCC1' 

select SUM(AMOUNT),LevelID,b.SegmentID,FiscalYear,MIN(l.limit)
from tbl_CREDITRISK_annualPurchasesBreakdown b 
JOIN tbl_CREDITRISK_creditLimit l on l.PK = b.PrimaryKey  
where LevelID = '5000004374' and l.SegmentID = 'GCC3' --and l.LimitLevel = 'Customer Group'
group by LevelID,b.SegmentID,FiscalYear--,l.Limit

**/






/*

select * from tbl_CREDITRISK_creditLimit where LevelID = '5000003498' and LimitLevel = 'Customer Group'
select SUM(Amount),SiteID,SegmentID 

select SUM(AMOUNT),LevelID,b.SegmentID,FiscalYear,l.Limit
from tbl_CREDITRISK_annualPurchasesBreakdown b 
JOIN tbl_CREDITRISK_creditLimit l on l.PK = b.PrimaryKey  
where LevelID = '5000003498' and l.SegmentID = 'GCC1' and l.LimitLevel = 'Customer Group'
group by LevelID,b.SegmentID,FiscalYear,l.Limit


select * from tbl_CREDITRISK_creditLimit where LevelID = '5000003498' and SegmentID = 'GCC1' and LimitLevel = 'Customer Group'

select * from tbl_CREDITRISK_annualPurchasesBreakdown 

--group by SiteID,SegmentID
45,128,484

select * from tbl_CREDITRISK_customer where LevelID = '5000033918'
select COUNT(distinct PK) from tbl_CREDITRISK_creditSegment

select COUNT(distinct SiteID) from tbl_CREDITRISK_creditSegment s
JOIN tbl_CREDITRISK_customer c on s.SiteID = c.PK
**/







