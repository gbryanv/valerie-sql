
DROP TABLE CASHFLOW_FORECAST_MAIN
DROP TABLE CASHFLOW_FORECAST_MAIN_UNPIVOT
DROP TABLE CASHFLOW_FORECAST_MAIN_DIFF

SELECT
     
     CF_JOINED.*
	 
INTO CASHFLOW_FORECAST_MAIN
FROM


(  

SELECT 
 
        CF_PERIOD.[FAS_Code] AS [FAS CODE],
        SUBSTRING(Package_period,4,4) + ' ' + SUBSTRING(Package_period,9,2) AS [FORECAST VERSION],
		SUBSTRING(Package_period,4,4) AS [FORECAST VERSION YEAR],
		SUBSTRING(Package_period,9,2) AS [FORECAST VERSION PERIOD],
		CASE SUBSTRING(Package_period,9,2) WHEN '01' THEN 1 WHEN '02' THEN 1 WHEN '03' THEN 1 
											WHEN '04' THEN 2 WHEN '05' THEN 2 WHEN '06' THEN 2 
											WHEN '07' THEN 3 WHEN '08' THEN 3 WHEN '09' THEN 3 
											WHEN '10' THEN 4 WHEN '11' THEN 4 WHEN '12' THEN 4 
											END [FORECAST VERSION QUARTER],
		CASE WHEN SUBSTRING(Package_period,4,4) = SUBSTRING(Forecast_period,4,4) AND SUBSTRING(Forecast_period,9,2) < SUBSTRING(Package_period,9,2) THEN 'ACTUAL' ELSE 'FORECAST' END [FORECAST TYPE],
		CASE WHEN SUBSTRING(Package_period,4,4) = SUBSTRING(Forecast_period,4,4) AND SUBSTRING(Forecast_period,9,2) < SUBSTRING(Package_period,9,2) THEN 1 ELSE 2 END [FORECAST TYPE NUM],
        SUBSTRING(Forecast_period,4,4)  AS [FORECAST YEAR],
		SUBSTRING(Forecast_period,9,2)  AS [FORECAST PERIOD],
		CASE SUBSTRING(Forecast_period,9,2) WHEN '01' THEN 1 WHEN '02' THEN 1 WHEN '03' THEN 1 
											WHEN '04' THEN 2 WHEN '05' THEN 2 WHEN '06' THEN 2 
											WHEN '07' THEN 3 WHEN '08' THEN 3 WHEN '09' THEN 3 
											WHEN '10' THEN 4 WHEN '11' THEN 4 WHEN '12' THEN 4 
											END [FORECAST QUARTER],
		ROUND(CAST(REPLACE([Opening_Cash_TLF],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [OPENING CASH TLF EUR],
		ROUND(CAST(REPLACE([Opening_Cash_TLG],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [OPENING CASH TLG EUR],
		ROUND(CAST(REPLACE([Opening_Cash_Banks],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [OPENING CASH BANKS EUR],
		ROUND(CAST(REPLACE([Opening_Bank_deposits],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [OPENING BANK DEPOSITS EUR],
		ROUND(CAST(REPLACE([Opening_Borrowing_TLF],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [OPENING BORRROWING TLF EUR],
		ROUND(CAST(REPLACE([Opening_Borrowing_TLG],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [OPENING BORRROWING TLG EUR],
		ROUND(CAST(REPLACE([Opening_Borrowing_Banks],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [OPENING OPENING BORRROWING BANKS EUR],
		ROUND(CAST(REPLACE([Opening_NFP],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [OPENING NFP EUR],
        ROUND(CAST(REPLACE([Collections_inside_Netting],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [COLLECTIONS INSIDE NETTING EUR],
		ROUND(CAST(REPLACE([Collections_outside_Netting],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [COLLECTIONS OUTSIDE NETTING EUR],
		ROUND(CAST(REPLACE([Collections___External_trade_receivables],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [COLLECTIONS EXTERNAL TRADE RECEIVABLES EUR],
		ROUND(CAST(REPLACE([Sales_of_capital_equipment],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [SALES OF CAPITAL EQUIPMENT EUR],
		ROUND(CAST(REPLACE([External_divestments],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [EXTERNAL DIVESTMENTS EUR],
		ROUND(CAST(REPLACE([Income_taxes_refunds],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [INCOME TAXES REFUNDS EUR],
		ROUND(CAST(REPLACE([VAT_refunds],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [VAT REFUNDS EUR],
		ROUND(CAST(REPLACE([Other_taxes_and_duties_refunds],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [OTHER TAXES AND DUTIES REFUNDS EUR],
		ROUND(CAST(REPLACE([Dividend_collected___Capital_increase],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [DIVIDEND COLLECTED CAPITAL INCREASE EUR],
		ROUND(CAST(REPLACE([Royalties_collected],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [ROYALTIES COLLECTED EUR],
		ROUND(CAST(REPLACE([Financial_inflows___Internal],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [FINANCIAL INFLOWS INTERNAL EUR],
		ROUND(CAST(REPLACE([Financial_inflows___External],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [FINANCIAL INFLOWS EXTERNAL EUR],
		ROUND(CAST(REPLACE([Other_cash_inflows___Internal],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [OTHER CASH INFLOWS INTERNAL EUR],
		ROUND(CAST(REPLACE([Other_cash_inflows___External],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [OTHER CASH INFLOWS EXTERNAL EUR],
		ROUND(CAST(REPLACE([Total_Inflows],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [TOTAL INFLOWS EUR],
		ROUND(CAST(REPLACE([Payments_inside_Netting],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [PAYMENTS INSIDE NETTING EUR],
		ROUND(CAST(REPLACE([Payments_outside_Netting],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [PAYMENTS OUTSIDE NETTING EUR],
		ROUND(CAST(REPLACE([Payments___External_trade__payables],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [PAYMENTS EXTERNAL TRADE PAYBABLES],
		ROUND(CAST(REPLACE([_Discounts__rebates__market_support_paid_],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [DISCOUNTS REBATS MARKET SUPPORT PAID EUR],
		ROUND(CAST(REPLACE([External_Investments],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [EXTERNAL INVESTMENTS EUR],
		ROUND(CAST(REPLACE([Income_taxes_paid],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [INCOME TAXES PAID EUR],
		ROUND(CAST(REPLACE([VAT_paid],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [VAT PAID EUR],
		ROUND(CAST(REPLACE([Other_taxes_and_duties_paid],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [OTHER TAXES AND DUTIES PAID EUR],
		ROUND(CAST(REPLACE([Dividend_paid___capital_reduction],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [DIVIDEND PAID CAPITAL REDUCTION EUR],
		ROUND(CAST(REPLACE([Royalties_paid],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [ROYALTIES PAID EUR],
        ROUND(CAST(REPLACE([_Salaries__social_securities_and_pensions_],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [SALARIES SOCIAL SECURITIES AND PENSIONS EUR],
		ROUND(CAST(REPLACE([Financial_outflows___external],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [FINANCIAL OUTFLOWS INTERNAL EUR],
		ROUND(CAST(REPLACE([Financial_outflows___Internal],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [FINANCIAL OUTFLOWS EXTERNAL EUR],	
		ROUND(CAST(REPLACE([Other_cash_outflows___Internal],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [OTHER CASH OUTFLOWS INTERNAL EUR],
		ROUND(CAST(REPLACE([Other_cash_outflows___External],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [OTHER CASH OUTFLOWS EXTERNAL EUR],
		ROUND(CAST(REPLACE([Total_Ouflows],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [TOTAL OUTFLOWS EUR],
		ROUND(CAST(REPLACE([Net_cash_flow],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [NET CASH FLOW EUR],
		ROUND(CAST(REPLACE([Closing_Cash_TLF],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [CLOSING CASH TLF EUR],
		ROUND(CAST(REPLACE([Closing_Cash_TLG],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [CLOSING CASH TLG EUR],
		ROUND(CAST(REPLACE([Closing_Cash_Banks],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [CLOSING CASH BANKS EUR],
		ROUND(CAST(REPLACE([Closing_Bank_deposits],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [CLOSING BANK DEPOSITS EUR],
		ROUND(CAST(REPLACE([Closing_Borrowing_TLF],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [CLOSING BORROWING TLF EUR],
		ROUND(CAST(REPLACE([Closing_Borrowing_TLG],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [CLOSING BORROWING TLG EUR],
		ROUND(CAST(REPLACE([Closing_Borrowing_Banks],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [CLOSING BORROWING BANKS EUR],
		ROUND(CAST(REPLACE([Total_Net],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [TOTAL NET EUR],
		ROUND(CAST(REPLACE([Peak_financing],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [PEAK FINANCING EUR],
		ROUND(CAST(REPLACE([Positive_balance_of_cash_pool_members],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [POSITIVE BALANCE CASH POOL MEMBERS],
		ROUND(CAST(REPLACE([Total_Peak],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [TOTAL PEAK EUR],
		ROUND(CAST(REPLACE([Net_cash_flow],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [NET FLOWS EUR],
		ROUND(CAST(REPLACE([Total_Inflows],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) + ABS(ROUND(CAST(REPLACE([Total_Ouflows],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0)) AS [GROSS FLOWS EUR],
		ROUND(CAST(REPLACE([Net_cash_flow],',','') as bigint) + CAST(REPLACE([Dividend_paid___capital_reduction],',','') as bigint) - CAST(REPLACE([Dividend_collected___Capital_increase] ,',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [NET W/O DIVIDEND FLOWS EUR],
        ROUND(CAST(REPLACE([Total_Inflows],',','') as bigint) - CAST(REPLACE([Dividend_collected___Capital_increase],',','') as bigint)/CAST([FX_Rates] AS FLOAT) + ABS(CAST(REPLACE([Total_Ouflows],',','') as bigint) + CAST(REPLACE([Dividend_paid___capital_reduction],',','') as bigint))/CAST([FX_Rates] AS FLOAT),0) AS [GROSS W/O DIVIDEND FLOWS EUR]
        
		FROM [Tetra_AnalyticsDB].[dbo].[CF_2018_11] CF_PERIOD
	
UNION ALL

SELECT 
        CF_PERIOD.[FAS_Code] AS [FAS CODE],
        SUBSTRING(Package_period,4,4) + ' ' + SUBSTRING(Package_period,9,2) AS [FORECAST VERSION],
		SUBSTRING(Package_period,4,4) AS [FORECAST VERSION YEAR],
		SUBSTRING(Package_period,9,2) AS [FORECAST VERSION PERIOD],
		CASE SUBSTRING(Package_period,9,2) WHEN '01' THEN 1 WHEN '02' THEN 1 WHEN '03' THEN 1 
											WHEN '04' THEN 2 WHEN '05' THEN 2 WHEN '06' THEN 2 
											WHEN '07' THEN 3 WHEN '08' THEN 3 WHEN '09' THEN 3 
											WHEN '10' THEN 4 WHEN '11' THEN 4 WHEN '12' THEN 4 
											END [FORECAST VERSION QUARTER],
		CASE WHEN SUBSTRING(Package_period,4,4) = SUBSTRING(Forecast_period,4,4) AND SUBSTRING(Forecast_period,9,2) < SUBSTRING(Package_period,9,2) THEN 'ACTUAL' ELSE 'FORECAST' END [FORECAST TYPE],
		CASE WHEN SUBSTRING(Package_period,4,4) = SUBSTRING(Forecast_period,4,4) AND SUBSTRING(Forecast_period,9,2) < SUBSTRING(Package_period,9,2) THEN 1 ELSE 2 END [FORECAST TYPE NUM],
        SUBSTRING(Forecast_period,4,4)  AS [FORECAST YEAR],
		SUBSTRING(Forecast_period,9,2)  AS [FORECAST PERIOD],
		CASE SUBSTRING(Forecast_period,9,2) WHEN '01' THEN 1 WHEN '02' THEN 1 WHEN '03' THEN 1 
											WHEN '04' THEN 2 WHEN '05' THEN 2 WHEN '06' THEN 2 
											WHEN '07' THEN 3 WHEN '08' THEN 3 WHEN '09' THEN 3 
											WHEN '10' THEN 4 WHEN '11' THEN 4 WHEN '12' THEN 4 
											END [FORECAST QUARTER],
		ROUND(CAST(REPLACE([Opening_Cash_TLF],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [OPENING CASH TLF EUR],
		ROUND(CAST(REPLACE([Opening_Cash_TLG],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [OPENING CASH TLG EUR],
		ROUND(CAST(REPLACE([Opening_Cash_Banks],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [OPENING CASH BANKS EUR],
		ROUND(CAST(REPLACE([Opening_Bank_deposits],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [OPENING BANK DEPOSITS EUR],
		ROUND(CAST(REPLACE([Opening_Borrowing_TLF],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [OPENING BORRROWING TLF EUR],
		ROUND(CAST(REPLACE([Opening_Borrowing_TLG],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [OPENING BORRROWING TLG EUR],
		ROUND(CAST(REPLACE([Opening_Borrowing_Banks],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [OPENING OPENING BORRROWING BANKS EUR],
		ROUND(CAST(REPLACE([Opening_NFP],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [OPENING NFP EUR],
        ROUND(CAST(REPLACE([Collections_inside_Netting],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [COLLECTIONS INSIDE NETTING EUR],
		ROUND(CAST(REPLACE([Collections_outside_Netting],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [COLLECTIONS OUTSIDE NETTING EUR],
		ROUND(CAST(REPLACE([Collections___External_trade_receivables],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [COLLECTIONS EXTERNAL TRADE RECEIVABLES EUR],
		ROUND(CAST(REPLACE([Sales_of_capital_equipment],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [SALES OF CAPITAL EQUIPMENT EUR],
		ROUND(CAST(REPLACE([External_divestments],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [EXTERNAL DIVESTMENTS EUR],
		ROUND(CAST(REPLACE([Income_taxes_refunds],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [INCOME TAXES REFUNDS EUR],
		ROUND(CAST(REPLACE([VAT_refunds],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [VAT REFUNDS EUR],
		ROUND(CAST(REPLACE([Other_taxes_and_duties_refunds],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [OTHER TAXES AND DUTIES REFUNDS EUR],
		ROUND(CAST(REPLACE([Dividend_collected___Capital_increase],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [DIVIDEND COLLECTED CAPITAL INCREASE EUR],
		ROUND(CAST(REPLACE([Royalties_collected],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [ROYALTIES COLLECTED EUR],
		ROUND(CAST(REPLACE([Financial_inflows___Internal],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [FINANCIAL INFLOWS INTERNAL EUR],
		ROUND(CAST(REPLACE([Financial_inflows___External],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [FINANCIAL INFLOWS EXTERNAL EUR],
		ROUND(CAST(REPLACE([Other_cash_inflows___Internal],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [OTHER CASH INFLOWS INTERNAL EUR],
		ROUND(CAST(REPLACE([Other_cash_inflows___External],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [OTHER CASH INFLOWS EXTERNAL EUR],
		ROUND(CAST(REPLACE([Total_Inflows],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [TOTAL INFLOWS EUR],
		ROUND(CAST(REPLACE([Payments_inside_Netting],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [PAYMENTS INSIDE NETTING EUR],
		ROUND(CAST(REPLACE([Payments_outside_Netting],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [PAYMENTS OUTSIDE NETTING EUR],
		ROUND(CAST(REPLACE([Payments___External_trade__payables],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [PAYMENTS EXTERNAL TRADE PAYBABLES],
		ROUND(CAST(REPLACE([_Discounts__rebates__market_support_paid_],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [DISCOUNTS REBATS MARKET SUPPORT PAID EUR],
		ROUND(CAST(REPLACE([External_Investments],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [EXTERNAL INVESTMENTS EUR],
		ROUND(CAST(REPLACE([Income_taxes_paid],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [INCOME TAXES PAID EUR],
		ROUND(CAST(REPLACE([VAT_paid],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [VAT PAID EUR],
		ROUND(CAST(REPLACE([Other_taxes_and_duties_paid],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [OTHER TAXES AND DUTIES PAID EUR],
		ROUND(CAST(REPLACE([Dividend_paid___capital_reduction],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [DIVIDEND PAID CAPITAL REDUCTION EUR],
		ROUND(CAST(REPLACE([Royalties_paid],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [ROYALTIES PAID EUR],
        ROUND(CAST(REPLACE([_Salaries__social_securities_and_pensions_],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [SALARIES SOCIAL SECURITIES AND PENSIONS EUR],
		ROUND(CAST(REPLACE([Financial_outflows___external],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [FINANCIAL OUTFLOWS INTERNAL EUR],
		ROUND(CAST(REPLACE([Financial_outflows___Internal],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [FINANCIAL OUTFLOWS EXTERNAL EUR],	
		ROUND(CAST(REPLACE([Other_cash_outflows___Internal],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [OTHER CASH OUTFLOWS INTERNAL EUR],
		ROUND(CAST(REPLACE([Other_cash_outflows___External],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [OTHER CASH OUTFLOWS EXTERNAL EUR],
		ROUND(CAST(REPLACE([Total_Ouflows],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [TOTAL OUTFLOWS EUR],
		ROUND(CAST(REPLACE([Net_cash_flow],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [NET CASH FLOW EUR],
		ROUND(CAST(REPLACE([Closing_Cash_TLF],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [CLOSING CASH TLF EUR],
		ROUND(CAST(REPLACE([Closing_Cash_TLG],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [CLOSING CASH TLG EUR],
		ROUND(CAST(REPLACE([Closing_Cash_Banks],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [CLOSING CASH BANKS EUR],
		ROUND(CAST(REPLACE([Closing_Bank_deposits],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [CLOSING BANK DEPOSITS EUR],
		ROUND(CAST(REPLACE([Closing_Borrowing_TLF],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [CLOSING BORROWING TLF EUR],
		ROUND(CAST(REPLACE([Closing_Borrowing_TLG],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [CLOSING BORROWING TLG EUR],
		ROUND(CAST(REPLACE([Closing_Borrowing_Banks],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [CLOSING BORROWING BANKS EUR],
		ROUND(CAST(REPLACE([Total_Net],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [TOTAL NET EUR],
		ROUND(CAST(REPLACE([Peak_financing],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [PEAK FINANCING EUR],
		ROUND(CAST(REPLACE([Positive_balance_of_cash_pool_members],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [POSITIVE BALANCE CASH POOL MEMBERS],
		ROUND(CAST(REPLACE([Total_Peak],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [TOTAL PEAK EUR],
		ROUND(CAST(REPLACE([Net_cash_flow],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [NET FLOWS EUR],
	    ROUND(CAST(REPLACE([Total_Inflows],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) + ABS(ROUND(CAST(REPLACE([Total_Ouflows],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0)) AS [GROSS FLOWS EUR],
		ROUND(CAST(REPLACE([Net_cash_flow],',','') as bigint) + CAST(REPLACE([Dividend_paid___capital_reduction],',','') as bigint) - CAST(REPLACE([Dividend_collected___Capital_increase] ,',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [NET W/O DIVIDEND FLOWS EUR],
        ROUND(CAST(REPLACE([Total_Inflows],',','') as bigint) - CAST(REPLACE([Dividend_collected___Capital_increase],',','') as bigint)/CAST([FX_Rates] AS FLOAT) + ABS(CAST(REPLACE([Total_Ouflows],',','') as bigint) + CAST(REPLACE([Dividend_paid___capital_reduction],',','') as bigint))/CAST([FX_Rates] AS FLOAT),0) AS [GROSS W/O DIVIDEND FLOWS EUR]

FROM [Tetra_AnalyticsDB].[dbo].[CF_2018_12] CF_PERIOD



UNION ALL
SELECT
	
  CF_PERIOD.[FAS_Code] AS [FAS CODE],
        SUBSTRING(Package_period,4,4) + ' ' + SUBSTRING(Package_period,9,2) AS [FORECAST VERSION],
		SUBSTRING(Package_period,4,4) AS [FORECAST VERSION YEAR],
		SUBSTRING(Package_period,9,2) AS [FORECAST VERSION PERIOD],
		CASE SUBSTRING(Package_period,9,2) WHEN '01' THEN 1 WHEN '02' THEN 1 WHEN '03' THEN 1 
											WHEN '04' THEN 2 WHEN '05' THEN 2 WHEN '06' THEN 2 
											WHEN '07' THEN 3 WHEN '08' THEN 3 WHEN '09' THEN 3 
											WHEN '10' THEN 4 WHEN '11' THEN 4 WHEN '12' THEN 4 
											END [FORECAST VERSION QUARTER],
		CASE WHEN SUBSTRING(Package_period,4,4) = SUBSTRING(Forecast_period,4,4) AND SUBSTRING(Forecast_period,9,2) < SUBSTRING(Package_period,9,2) THEN 'ACTUAL' ELSE 'FORECAST' END [FORECAST TYPE],
		CASE WHEN SUBSTRING(Package_period,4,4) = SUBSTRING(Forecast_period,4,4) AND SUBSTRING(Forecast_period,9,2) < SUBSTRING(Package_period,9,2) THEN 1 ELSE 2 END [FORECAST TYPE NUM],
        SUBSTRING(Forecast_period,4,4)  AS [FORECAST YEAR],
		SUBSTRING(Forecast_period,9,2)  AS [FORECAST PERIOD],
		CASE SUBSTRING(Forecast_period,9,2) WHEN '01' THEN 1 WHEN '02' THEN 1 WHEN '03' THEN 1 
											WHEN '04' THEN 2 WHEN '05' THEN 2 WHEN '06' THEN 2 
											WHEN '07' THEN 3 WHEN '08' THEN 3 WHEN '09' THEN 3 
											WHEN '10' THEN 4 WHEN '11' THEN 4 WHEN '12' THEN 4 
											END [FORECAST QUARTER],
		ROUND(CAST(REPLACE([Opening_Cash_TLF],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [OPENING CASH TLF EUR],
		ROUND(CAST(REPLACE([Opening_Cash_TLG],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [OPENING CASH TLG EUR],
		ROUND(CAST(REPLACE([Opening_Cash_Banks],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [OPENING CASH BANKS EUR],
		ROUND(CAST(REPLACE([Opening_Bank_deposits],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [OPENING BANK DEPOSITS EUR],
		ROUND(CAST(REPLACE([Opening_Borrowing_TLF],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [OPENING BORRROWING TLF EUR],
		ROUND(CAST(REPLACE([Opening_Borrowing_TLG],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [OPENING BORRROWING TLG EUR],
		ROUND(CAST(REPLACE([Opening_Borrowing_Banks],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [OPENING OPENING BORRROWING BANKS EUR],
		ROUND(CAST(REPLACE([Opening_NFP],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [OPENING NFP EUR],
        ROUND(CAST(REPLACE([Collections_inside_Netting],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [COLLECTIONS INSIDE NETTING EUR],
		ROUND(CAST(REPLACE([Collections_outside_Netting],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [COLLECTIONS OUTSIDE NETTING EUR],
		ROUND(CAST(REPLACE([Collections___External_trade_receivables],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [COLLECTIONS EXTERNAL TRADE RECEIVABLES EUR],
		ROUND(CAST(REPLACE([Sales_of_capital_equipment],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [SALES OF CAPITAL EQUIPMENT EUR],
		ROUND(CAST(REPLACE([External_divestments],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [EXTERNAL DIVESTMENTS EUR],
		ROUND(CAST(REPLACE([Income_taxes_refunds],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [INCOME TAXES REFUNDS EUR],
		ROUND(CAST(REPLACE([VAT_refunds],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [VAT REFUNDS EUR],
		ROUND(CAST(REPLACE([Other_taxes_and_duties_refunds],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [OTHER TAXES AND DUTIES REFUNDS EUR],
		ROUND(CAST(REPLACE([Dividend_collected___Capital_increase],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [DIVIDEND COLLECTED CAPITAL INCREASE EUR],
		ROUND(CAST(REPLACE([Royalties_collected],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [ROYALTIES COLLECTED EUR],
		ROUND(CAST(REPLACE([Financial_inflows___Internal],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [FINANCIAL INFLOWS INTERNAL EUR],
		ROUND(CAST(REPLACE([Financial_inflows___External],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [FINANCIAL INFLOWS EXTERNAL EUR],
		ROUND(CAST(REPLACE([Other_cash_inflows___Internal],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [OTHER CASH INFLOWS INTERNAL EUR],
		ROUND(CAST(REPLACE([Other_cash_inflows___External],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [OTHER CASH INFLOWS EXTERNAL EUR],
		ROUND(CAST(REPLACE([Total_Inflows],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [TOTAL INFLOWS EUR],
		ROUND(CAST(REPLACE([Payments_inside_Netting],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [PAYMENTS INSIDE NETTING EUR],
		ROUND(CAST(REPLACE([Payments_outside_Netting],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [PAYMENTS OUTSIDE NETTING EUR],
		ROUND(CAST(REPLACE([Payments___External_trade__payables],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [PAYMENTS EXTERNAL TRADE PAYBABLES],
		ROUND(CAST(REPLACE([_Discounts__rebates__market_support_paid_],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [DISCOUNTS REBATS MARKET SUPPORT PAID EUR],
		ROUND(CAST(REPLACE([External_Investments],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [EXTERNAL INVESTMENTS EUR],
		ROUND(CAST(REPLACE([Income_taxes_paid],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [INCOME TAXES PAID EUR],
		ROUND(CAST(REPLACE([VAT_paid],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [VAT PAID EUR],
		ROUND(CAST(REPLACE([Other_taxes_and_duties_paid],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [OTHER TAXES AND DUTIES PAID EUR],
		ROUND(CAST(REPLACE([Dividend_paid___capital_reduction],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [DIVIDEND PAID CAPITAL REDUCTION EUR],
		ROUND(CAST(REPLACE([Royalties_paid],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [ROYALTIES PAID EUR],
        ROUND(CAST(REPLACE([_Salaries__social_securities_and_pensions_],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [SALARIES SOCIAL SECURITIES AND PENSIONS EUR],
		ROUND(CAST(REPLACE([Financial_outflows___external],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [FINANCIAL OUTFLOWS INTERNAL EUR],
		ROUND(CAST(REPLACE([Financial_outflows___Internal],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [FINANCIAL OUTFLOWS EXTERNAL EUR],	
		ROUND(CAST(REPLACE([Other_cash_outflows___Internal],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [OTHER CASH OUTFLOWS INTERNAL EUR],
		ROUND(CAST(REPLACE([Other_cash_outflows___External],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [OTHER CASH OUTFLOWS EXTERNAL EUR],
		ROUND(CAST(REPLACE([Total_Ouflows],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [TOTAL OUTFLOWS EUR],
		ROUND(CAST(REPLACE([Net_cash_flow],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [NET CASH FLOW EUR],
		ROUND(CAST(REPLACE([Closing_Cash_TLF],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [CLOSING CASH TLF EUR],
		ROUND(CAST(REPLACE([Closing_Cash_TLG],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [CLOSING CASH TLG EUR],
		ROUND(CAST(REPLACE([Closing_Cash_Banks],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [CLOSING CASH BANKS EUR],
		ROUND(CAST(REPLACE([Closing_Bank_deposits],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [CLOSING BANK DEPOSITS EUR],
		ROUND(CAST(REPLACE([Closing_Borrowing_TLF],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [CLOSING BORROWING TLF EUR],
		ROUND(CAST(REPLACE([Closing_Borrowing_TLG],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [CLOSING BORROWING TLG EUR],
		ROUND(CAST(REPLACE([Closing_Borrowing_Banks],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [CLOSING BORROWING BANKS EUR],
		ROUND(CAST(REPLACE([Total_Net],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [TOTAL NET EUR],
		ROUND(CAST(REPLACE([Peak_financing],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [PEAK FINANCING EUR],
		ROUND(CAST(REPLACE([Positive_balance_of_cash_pool_members],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [POSITIVE BALANCE CASH POOL MEMBERS],
		ROUND(CAST(REPLACE([Total_Peak],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [TOTAL PEAK EUR],
		ROUND(CAST(REPLACE([Net_cash_flow],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [NET FLOWS EUR],
		ROUND(CAST(REPLACE([Total_Inflows],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) + ABS(ROUND(CAST(REPLACE([Total_Ouflows],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0)) AS [GROSS FLOWS EUR],
		ROUND(CAST(REPLACE([Net_cash_flow],',','') as bigint) + CAST(REPLACE([Dividend_paid___capital_reduction],',','') as bigint) - CAST(REPLACE([Dividend_collected___Capital_increase] ,',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [NET W/O DIVIDEND FLOWS EUR],
        ROUND(CAST(REPLACE([Total_Inflows],',','') as bigint) - CAST(REPLACE([Dividend_collected___Capital_increase],',','') as bigint)/CAST([FX_Rates] AS FLOAT) + ABS(CAST(REPLACE([Total_Ouflows],',','') as bigint) + CAST(REPLACE([Dividend_paid___capital_reduction],',','') as bigint))/CAST([FX_Rates] AS FLOAT),0) AS [GROSS W/O DIVIDEND FLOWS EUR]
        
		FROM [Tetra_AnalyticsDB].[dbo].[CF_2019_01] CF_PERIOD

UNION ALL
SELECT
	
CF_PERIOD.[FAS_Code] AS [FAS CODE],
        SUBSTRING(Package_period,4,4) + ' ' + SUBSTRING(Package_period,9,2) AS [FORECAST VERSION],
		SUBSTRING(Package_period,4,4) AS [FORECAST VERSION YEAR],
		SUBSTRING(Package_period,9,2) AS [FORECAST VERSION PERIOD],
		CASE SUBSTRING(Package_period,9,2) WHEN '01' THEN 1 WHEN '02' THEN 1 WHEN '03' THEN 1 
											WHEN '04' THEN 2 WHEN '05' THEN 2 WHEN '06' THEN 2 
											WHEN '07' THEN 3 WHEN '08' THEN 3 WHEN '09' THEN 3 
											WHEN '10' THEN 4 WHEN '11' THEN 4 WHEN '12' THEN 4 
											END [FORECAST VERSION QUARTER],
		CASE WHEN SUBSTRING(Package_period,4,4) = SUBSTRING(Forecast_period,4,4) AND SUBSTRING(Forecast_period,9,2) < SUBSTRING(Package_period,9,2) THEN 'ACTUAL' ELSE 'FORECAST' END [FORECAST TYPE],
		CASE WHEN SUBSTRING(Package_period,4,4) = SUBSTRING(Forecast_period,4,4) AND SUBSTRING(Forecast_period,9,2) < SUBSTRING(Package_period,9,2) THEN 1 ELSE 2 END [FORECAST TYPE NUM],
        SUBSTRING(Forecast_period,4,4)  AS [FORECAST YEAR],
		SUBSTRING(Forecast_period,9,2)  AS [FORECAST PERIOD],
		CASE SUBSTRING(Forecast_period,9,2) WHEN '01' THEN 1 WHEN '02' THEN 1 WHEN '03' THEN 1 
											WHEN '04' THEN 2 WHEN '05' THEN 2 WHEN '06' THEN 2 
											WHEN '07' THEN 3 WHEN '08' THEN 3 WHEN '09' THEN 3 
											WHEN '10' THEN 4 WHEN '11' THEN 4 WHEN '12' THEN 4 
											END [FORECAST QUARTER],
		ROUND(CAST(REPLACE([Opening_Cash_TLF],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [OPENING CASH TLF EUR],
		ROUND(CAST(REPLACE([Opening_Cash_TLG],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [OPENING CASH TLG EUR],
		ROUND(CAST(REPLACE([Opening_Cash_Banks],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [OPENING CASH BANKS EUR],
		ROUND(CAST(REPLACE([Opening_Bank_deposits],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [OPENING BANK DEPOSITS EUR],
		ROUND(CAST(REPLACE([Opening_Borrowing_TLF],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [OPENING BORRROWING TLF EUR],
		ROUND(CAST(REPLACE([Opening_Borrowing_TLG],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [OPENING BORRROWING TLG EUR],
		ROUND(CAST(REPLACE([Opening_Borrowing_Banks],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [OPENING OPENING BORRROWING BANKS EUR],
		ROUND(CAST(REPLACE([Opening_NFP],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [OPENING NFP EUR],
        ROUND(CAST(REPLACE([Collections_inside_Netting],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [COLLECTIONS INSIDE NETTING EUR],
		ROUND(CAST(REPLACE([Collections_outside_Netting],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [COLLECTIONS OUTSIDE NETTING EUR],
		ROUND(CAST(REPLACE([Collections___External_trade_receivables],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [COLLECTIONS EXTERNAL TRADE RECEIVABLES EUR],
		ROUND(CAST(REPLACE([Sales_of_capital_equipment],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [SALES OF CAPITAL EQUIPMENT EUR],
		ROUND(CAST(REPLACE([External_divestments],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [EXTERNAL DIVESTMENTS EUR],
		ROUND(CAST(REPLACE([Income_taxes_refunds],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [INCOME TAXES REFUNDS EUR],
		ROUND(CAST(REPLACE([VAT_refunds],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [VAT REFUNDS EUR],
		ROUND(CAST(REPLACE([Other_taxes_and_duties_refunds],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [OTHER TAXES AND DUTIES REFUNDS EUR],
		ROUND(CAST(REPLACE([Dividend_collected___Capital_increase],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [DIVIDEND COLLECTED CAPITAL INCREASE EUR],
		ROUND(CAST(REPLACE([Royalties_collected],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [ROYALTIES COLLECTED EUR],
		ROUND(CAST(REPLACE([Financial_inflows___Internal],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [FINANCIAL INFLOWS INTERNAL EUR],
		ROUND(CAST(REPLACE([Financial_inflows___External],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [FINANCIAL INFLOWS EXTERNAL EUR],
		ROUND(CAST(REPLACE([Other_cash_inflows___Internal],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [OTHER CASH INFLOWS INTERNAL EUR],
		ROUND(CAST(REPLACE([Other_cash_inflows___External],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [OTHER CASH INFLOWS EXTERNAL EUR],
		ROUND(CAST(REPLACE([Total_Inflows],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [TOTAL INFLOWS EUR],
		ROUND(CAST(REPLACE([Payments_inside_Netting],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [PAYMENTS INSIDE NETTING EUR],
		ROUND(CAST(REPLACE([Payments_outside_Netting],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [PAYMENTS OUTSIDE NETTING EUR],
		ROUND(CAST(REPLACE([Payments___External_trade__payables],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [PAYMENTS EXTERNAL TRADE PAYBABLES],
		ROUND(CAST(REPLACE([_Discounts__rebates__market_support_paid_],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [DISCOUNTS REBATS MARKET SUPPORT PAID EUR],
		ROUND(CAST(REPLACE([External_Investments],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [EXTERNAL INVESTMENTS EUR],
		ROUND(CAST(REPLACE([Income_taxes_paid],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [INCOME TAXES PAID EUR],
		ROUND(CAST(REPLACE([VAT_paid],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [VAT PAID EUR],
		ROUND(CAST(REPLACE([Other_taxes_and_duties_paid],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [OTHER TAXES AND DUTIES PAID EUR],
		ROUND(CAST(REPLACE([Dividend_paid___capital_reduction],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [DIVIDEND PAID CAPITAL REDUCTION EUR],
		ROUND(CAST(REPLACE([Royalties_paid],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [ROYALTIES PAID EUR],
        ROUND(CAST(REPLACE([_Salaries__social_securities_and_pensions_],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [SALARIES SOCIAL SECURITIES AND PENSIONS EUR],
		ROUND(CAST(REPLACE([Financial_outflows___external],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [FINANCIAL OUTFLOWS INTERNAL EUR],
		ROUND(CAST(REPLACE([Financial_outflows___Internal],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [FINANCIAL OUTFLOWS EXTERNAL EUR],	
		ROUND(CAST(REPLACE([Other_cash_outflows___Internal],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [OTHER CASH OUTFLOWS INTERNAL EUR],
		ROUND(CAST(REPLACE([Other_cash_outflows___External],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [OTHER CASH OUTFLOWS EXTERNAL EUR],
		ROUND(CAST(REPLACE([Total_Ouflows],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [TOTAL OUTFLOWS EUR],
		ROUND(CAST(REPLACE([Net_cash_flow],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [NET CASH FLOW EUR],
		ROUND(CAST(REPLACE([Closing_Cash_TLF],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [CLOSING CASH TLF EUR],
		ROUND(CAST(REPLACE([Closing_Cash_TLG],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [CLOSING CASH TLG EUR],
		ROUND(CAST(REPLACE([Closing_Cash_Banks],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [CLOSING CASH BANKS EUR],
		ROUND(CAST(REPLACE([Closing_Bank_deposits],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [CLOSING BANK DEPOSITS EUR],
		ROUND(CAST(REPLACE([Closing_Borrowing_TLF],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [CLOSING BORROWING TLF EUR],
		ROUND(CAST(REPLACE([Closing_Borrowing_TLG],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [CLOSING BORROWING TLG EUR],
		ROUND(CAST(REPLACE([Closing_Borrowing_Banks],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [CLOSING BORROWING BANKS EUR],
		ROUND(CAST(REPLACE([Total_Net],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [TOTAL NET EUR],
		ROUND(CAST(REPLACE([Peak_financing],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [PEAK FINANCING EUR],
		ROUND(CAST(REPLACE([Positive_balance_of_cash_pool_members],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [POSITIVE BALANCE CASH POOL MEMBERS],
		ROUND(CAST(REPLACE([Total_Peak],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [TOTAL PEAK EUR],
		ROUND(CAST(REPLACE([Net_cash_flow],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [NET FLOWS EUR],
        ROUND(CAST(REPLACE([Total_Inflows],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) + ABS(ROUND(CAST(REPLACE([Total_Ouflows],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0)) AS [GROSS FLOWS EUR],
		ROUND(CAST(REPLACE([Net_cash_flow],',','') as bigint) + CAST(REPLACE([Dividend_paid___capital_reduction],',','') as bigint) - CAST(REPLACE([Dividend_collected___Capital_increase] ,',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [NET W/O DIVIDEND FLOWS EUR],
        ROUND(CAST(REPLACE([Total_Inflows],',','') as bigint) - CAST(REPLACE([Dividend_collected___Capital_increase],',','') as bigint)/CAST([FX_Rates] AS FLOAT) + ABS(CAST(REPLACE([Total_Ouflows],',','') as bigint) + CAST(REPLACE([Dividend_paid___capital_reduction],',','') as bigint))/CAST([FX_Rates] AS FLOAT),0) AS [GROSS W/O DIVIDEND FLOWS EUR]


FROM [Tetra_AnalyticsDB].[dbo].[CF_2019_02] CF_PERIOD
		

UNION ALL

SELECT 

CF_PERIOD.[FAS_Code] AS [FAS CODE],
        SUBSTRING(Package_period,4,4) + ' ' + SUBSTRING(Package_period,9,2) AS [FORECAST VERSION],
		SUBSTRING(Package_period,4,4) AS [FORECAST VERSION YEAR],
		SUBSTRING(Package_period,9,2) AS [FORECAST VERSION PERIOD],
		CASE SUBSTRING(Package_period,9,2) WHEN '01' THEN 1 WHEN '02' THEN 1 WHEN '03' THEN 1 
											WHEN '04' THEN 2 WHEN '05' THEN 2 WHEN '06' THEN 2 
											WHEN '07' THEN 3 WHEN '08' THEN 3 WHEN '09' THEN 3 
											WHEN '10' THEN 4 WHEN '11' THEN 4 WHEN '12' THEN 4 
											END [FORECAST VERSION QUARTER],
		CASE WHEN SUBSTRING(Package_period,4,4) = SUBSTRING(Forecast_period,4,4) AND SUBSTRING(Forecast_period,9,2) < SUBSTRING(Package_period,9,2) THEN 'ACTUAL' ELSE 'FORECAST' END [FORECAST TYPE],
		CASE WHEN SUBSTRING(Package_period,4,4) = SUBSTRING(Forecast_period,4,4) AND SUBSTRING(Forecast_period,9,2) < SUBSTRING(Package_period,9,2) THEN 1 ELSE 2 END [FORECAST TYPE NUM],
        SUBSTRING(Forecast_period,4,4)  AS [FORECAST YEAR],
		SUBSTRING(Forecast_period,9,2)  AS [FORECAST PERIOD],
		CASE SUBSTRING(Forecast_period,9,2) WHEN '01' THEN 1 WHEN '02' THEN 1 WHEN '03' THEN 1 
											WHEN '04' THEN 2 WHEN '05' THEN 2 WHEN '06' THEN 2 
											WHEN '07' THEN 3 WHEN '08' THEN 3 WHEN '09' THEN 3 
											WHEN '10' THEN 4 WHEN '11' THEN 4 WHEN '12' THEN 4 
											END [FORECAST QUARTER],
		ROUND(CAST(REPLACE([Opening_Cash_TLF],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [OPENING CASH TLF EUR],
		ROUND(CAST(REPLACE([Opening_Cash_TLG],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [OPENING CASH TLG EUR],
		ROUND(CAST(REPLACE([Opening_Cash_Banks],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [OPENING CASH BANKS EUR],
		ROUND(CAST(REPLACE([Opening_Bank_deposits],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [OPENING BANK DEPOSITS EUR],
		ROUND(CAST(REPLACE([Opening_Borrowing_TLF],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [OPENING BORRROWING TLF EUR],
		ROUND(CAST(REPLACE([Opening_Borrowing_TLG],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [OPENING BORRROWING TLG EUR],
		ROUND(CAST(REPLACE([Opening_Borrowing_Banks],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [OPENING OPENING BORRROWING BANKS EUR],
		ROUND(CAST(REPLACE([Opening_NFP],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [OPENING NFP EUR],
        ROUND(CAST(REPLACE([Collections_inside_Netting],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [COLLECTIONS INSIDE NETTING EUR],
		ROUND(CAST(REPLACE([Collections_outside_Netting],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [COLLECTIONS OUTSIDE NETTING EUR],
		ROUND(CAST(REPLACE([Collections___External_trade_receivables],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [COLLECTIONS EXTERNAL TRADE RECEIVABLES EUR],
		ROUND(CAST(REPLACE([Sales_of_capital_equipment],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [SALES OF CAPITAL EQUIPMENT EUR],
		ROUND(CAST(REPLACE([External_divestments],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [EXTERNAL DIVESTMENTS EUR],
		ROUND(CAST(REPLACE([Income_taxes_refunds],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [INCOME TAXES REFUNDS EUR],
		ROUND(CAST(REPLACE([VAT_refunds],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [VAT REFUNDS EUR],
		ROUND(CAST(REPLACE([Other_taxes_and_duties_refunds],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [OTHER TAXES AND DUTIES REFUNDS EUR],
		ROUND(CAST(REPLACE([Dividend_collected___Capital_increase],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [DIVIDEND COLLECTED CAPITAL INCREASE EUR],
		ROUND(CAST(REPLACE([Royalties_collected],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [ROYALTIES COLLECTED EUR],
		ROUND(CAST(REPLACE([Financial_inflows___Internal],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [FINANCIAL INFLOWS INTERNAL EUR],
		ROUND(CAST(REPLACE([Financial_inflows___External],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [FINANCIAL INFLOWS EXTERNAL EUR],
		ROUND(CAST(REPLACE([Other_cash_inflows___Internal],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [OTHER CASH INFLOWS INTERNAL EUR],
		ROUND(CAST(REPLACE([Other_cash_inflows___External],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [OTHER CASH INFLOWS EXTERNAL EUR],
		ROUND(CAST(REPLACE([Total_Inflows],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [TOTAL INFLOWS EUR],
		ROUND(CAST(REPLACE([Payments_inside_Netting],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [PAYMENTS INSIDE NETTING EUR],
		ROUND(CAST(REPLACE([Payments_outside_Netting],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [PAYMENTS OUTSIDE NETTING EUR],
		ROUND(CAST(REPLACE([Payments___External_trade__payables],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [PAYMENTS EXTERNAL TRADE PAYBABLES],
		ROUND(CAST(REPLACE([_Discounts__rebates__market_support_paid_],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [DISCOUNTS REBATS MARKET SUPPORT PAID EUR],
		ROUND(CAST(REPLACE([External_Investments],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [EXTERNAL INVESTMENTS EUR],
		ROUND(CAST(REPLACE([Income_taxes_paid],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [INCOME TAXES PAID EUR],
		ROUND(CAST(REPLACE([VAT_paid],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [VAT PAID EUR],
		ROUND(CAST(REPLACE([Other_taxes_and_duties_paid],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [OTHER TAXES AND DUTIES PAID EUR],
		ROUND(CAST(REPLACE([Dividend_paid___capital_reduction],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [DIVIDEND PAID CAPITAL REDUCTION EUR],
		ROUND(CAST(REPLACE([Royalties_paid],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [ROYALTIES PAID EUR],
        ROUND(CAST(REPLACE([_Salaries__social_securities_and_pensions_],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [SALARIES SOCIAL SECURITIES AND PENSIONS EUR],
		ROUND(CAST(REPLACE([Financial_outflows___external],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [FINANCIAL OUTFLOWS INTERNAL EUR],
		ROUND(CAST(REPLACE([Financial_outflows___Internal],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [FINANCIAL OUTFLOWS EXTERNAL EUR],	
		ROUND(CAST(REPLACE([Other_cash_outflows___Internal],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [OTHER CASH OUTFLOWS INTERNAL EUR],
		ROUND(CAST(REPLACE([Other_cash_outflows___External],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [OTHER CASH OUTFLOWS EXTERNAL EUR],
		ROUND(CAST(REPLACE([Total_Ouflows],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [TOTAL OUTFLOWS EUR],
		ROUND(CAST(REPLACE([Net_cash_flow],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [NET CASH FLOW EUR],
		ROUND(CAST(REPLACE([Closing_Cash_TLF],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [CLOSING CASH TLF EUR],
		ROUND(CAST(REPLACE([Closing_Cash_TLG],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [CLOSING CASH TLG EUR],
		ROUND(CAST(REPLACE([Closing_Cash_Banks],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [CLOSING CASH BANKS EUR],
		ROUND(CAST(REPLACE([Closing_Bank_deposits],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [CLOSING BANK DEPOSITS EUR],
		ROUND(CAST(REPLACE([Closing_Borrowing_TLF],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [CLOSING BORROWING TLF EUR],
		ROUND(CAST(REPLACE([Closing_Borrowing_TLG],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [CLOSING BORROWING TLG EUR],
		ROUND(CAST(REPLACE([Closing_Borrowing_Banks],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [CLOSING BORROWING BANKS EUR],
		ROUND(CAST(REPLACE([Total_Net],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [TOTAL NET EUR],
		ROUND(CAST(REPLACE([Peak_financing],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [PEAK FINANCING EUR],
		ROUND(CAST(REPLACE([Positive_balance_of_cash_pool_members],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [POSITIVE BALANCE CASH POOL MEMBERS],
		ROUND(CAST(REPLACE([Total_Peak],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [TOTAL PEAK EUR],
		ROUND(CAST(REPLACE([Net_cash_flow],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [NET FLOWS EUR],
		ROUND(CAST(REPLACE([Total_Inflows],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) + ABS(ROUND(CAST(REPLACE([Total_Ouflows],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0)) AS [GROSS FLOWS EUR],
		ROUND(CAST(REPLACE([Net_cash_flow],',','') as bigint) + CAST(REPLACE([Dividend_paid___capital_reduction],',','') as bigint) - CAST(REPLACE([Dividend_collected___Capital_increase] ,',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [NET W/O DIVIDEND FLOWS EUR],
        ROUND(CAST(REPLACE([Total_Inflows],',','') as bigint) - CAST(REPLACE([Dividend_collected___Capital_increase],',','') as bigint)/CAST([FX_Rates] AS FLOAT) + ABS(CAST(REPLACE([Total_Ouflows],',','') as bigint) + CAST(REPLACE([Dividend_paid___capital_reduction],',','') as bigint))/CAST([FX_Rates] AS FLOAT),0) AS [GROSS W/O DIVIDEND FLOWS EUR]
FROM [Tetra_AnalyticsDB].[dbo].[CF_2019_03] CF_PERIOD
		


UNION ALL

SELECT 
     
CF_PERIOD.[FAS_Code] AS [FAS CODE],
        SUBSTRING(Package_period,4,4) + ' ' + SUBSTRING(Package_period,9,2) AS [FORECAST VERSION],
		SUBSTRING(Package_period,4,4) AS [FORECAST VERSION YEAR],
		SUBSTRING(Package_period,9,2) AS [FORECAST VERSION PERIOD],
		CASE SUBSTRING(Package_period,9,2) WHEN '01' THEN 1 WHEN '02' THEN 1 WHEN '03' THEN 1 
											WHEN '04' THEN 2 WHEN '05' THEN 2 WHEN '06' THEN 2 
											WHEN '07' THEN 3 WHEN '08' THEN 3 WHEN '09' THEN 3 
											WHEN '10' THEN 4 WHEN '11' THEN 4 WHEN '12' THEN 4 
											END [FORECAST VERSION QUARTER],
		CASE WHEN SUBSTRING(Package_period,4,4) = SUBSTRING(Forecast_period,4,4) AND SUBSTRING(Forecast_period,9,2) < SUBSTRING(Package_period,9,2) THEN 'ACTUAL' ELSE 'FORECAST' END [FORECAST TYPE],
		CASE WHEN SUBSTRING(Package_period,4,4) = SUBSTRING(Forecast_period,4,4) AND SUBSTRING(Forecast_period,9,2) < SUBSTRING(Package_period,9,2) THEN 1 ELSE 2 END [FORECAST TYPE NUM],
        SUBSTRING(Forecast_period,4,4)  AS [FORECAST YEAR],
		SUBSTRING(Forecast_period,9,2)  AS [FORECAST PERIOD],
		CASE SUBSTRING(Forecast_period,9,2) WHEN '01' THEN 1 WHEN '02' THEN 1 WHEN '03' THEN 1 
											WHEN '04' THEN 2 WHEN '05' THEN 2 WHEN '06' THEN 2 
											WHEN '07' THEN 3 WHEN '08' THEN 3 WHEN '09' THEN 3 
											WHEN '10' THEN 4 WHEN '11' THEN 4 WHEN '12' THEN 4 
											END [FORECAST QUARTER],
		ROUND(CAST(REPLACE([Opening_Cash_TLF],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [OPENING CASH TLF EUR],
		ROUND(CAST(REPLACE([Opening_Cash_TLG],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [OPENING CASH TLG EUR],
		ROUND(CAST(REPLACE([Opening_Cash_Banks],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [OPENING CASH BANKS EUR],
		ROUND(CAST(REPLACE([Opening_Bank_deposits],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [OPENING BANK DEPOSITS EUR],
		ROUND(CAST(REPLACE([Opening_Borrowing_TLF],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [OPENING BORRROWING TLF EUR],
		ROUND(CAST(REPLACE([Opening_Borrowing_TLG],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [OPENING BORRROWING TLG EUR],
		ROUND(CAST(REPLACE([Opening_Borrowing_Banks],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [OPENING OPENING BORRROWING BANKS EUR],
		ROUND(CAST(REPLACE([Opening_NFP],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [OPENING NFP EUR],
        ROUND(CAST(REPLACE([Collections_inside_Netting],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [COLLECTIONS INSIDE NETTING EUR],
		ROUND(CAST(REPLACE([Collections_outside_Netting],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [COLLECTIONS OUTSIDE NETTING EUR],
		ROUND(CAST(REPLACE([Collections___External_trade_receivables],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [COLLECTIONS EXTERNAL TRADE RECEIVABLES EUR],
		ROUND(CAST(REPLACE([Sales_of_capital_equipment],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [SALES OF CAPITAL EQUIPMENT EUR],
		ROUND(CAST(REPLACE([External_divestments],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [EXTERNAL DIVESTMENTS EUR],
		ROUND(CAST(REPLACE([Income_taxes_refunds],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [INCOME TAXES REFUNDS EUR],
		ROUND(CAST(REPLACE([VAT_refunds],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [VAT REFUNDS EUR],
		ROUND(CAST(REPLACE([Other_taxes_and_duties_refunds],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [OTHER TAXES AND DUTIES REFUNDS EUR],
		ROUND(CAST(REPLACE([Dividend_collected___Capital_increase],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [DIVIDEND COLLECTED CAPITAL INCREASE EUR],
		ROUND(CAST(REPLACE([Royalties_collected],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [ROYALTIES COLLECTED EUR],
		ROUND(CAST(REPLACE([Financial_inflows___Internal],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [FINANCIAL INFLOWS INTERNAL EUR],
		ROUND(CAST(REPLACE([Financial_inflows___External],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [FINANCIAL INFLOWS EXTERNAL EUR],
		ROUND(CAST(REPLACE([Other_cash_inflows___Internal],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [OTHER CASH INFLOWS INTERNAL EUR],
		ROUND(CAST(REPLACE([Other_cash_inflows___External],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [OTHER CASH INFLOWS EXTERNAL EUR],
		ROUND(CAST(REPLACE([Total_Inflows],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [TOTAL INFLOWS EUR],
		ROUND(CAST(REPLACE([Payments_inside_Netting],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [PAYMENTS INSIDE NETTING EUR],
		ROUND(CAST(REPLACE([Payments_outside_Netting],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [PAYMENTS OUTSIDE NETTING EUR],
		ROUND(CAST(REPLACE([Payments___External_trade__payables],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [PAYMENTS EXTERNAL TRADE PAYBABLES],
		ROUND(CAST(REPLACE([_Discounts__rebates__market_support_paid_],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [DISCOUNTS REBATS MARKET SUPPORT PAID EUR],
		ROUND(CAST(REPLACE([External_Investments],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [EXTERNAL INVESTMENTS EUR],
		ROUND(CAST(REPLACE([Income_taxes_paid],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [INCOME TAXES PAID EUR],
		ROUND(CAST(REPLACE([VAT_paid],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [VAT PAID EUR],
		ROUND(CAST(REPLACE([Other_taxes_and_duties_paid],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [OTHER TAXES AND DUTIES PAID EUR],
		ROUND(CAST(REPLACE([Dividend_paid___capital_reduction],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [DIVIDEND PAID CAPITAL REDUCTION EUR],
		ROUND(CAST(REPLACE([Royalties_paid],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [ROYALTIES PAID EUR],
        ROUND(CAST(REPLACE([_Salaries__social_securities_and_pensions_],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [SALARIES SOCIAL SECURITIES AND PENSIONS EUR],
		ROUND(CAST(REPLACE([Financial_outflows___external],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [FINANCIAL OUTFLOWS INTERNAL EUR],
		ROUND(CAST(REPLACE([Financial_outflows___Internal],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [FINANCIAL OUTFLOWS EXTERNAL EUR],	
		ROUND(CAST(REPLACE([Other_cash_outflows___Internal],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [OTHER CASH OUTFLOWS INTERNAL EUR],
		ROUND(CAST(REPLACE([Other_cash_outflows___External],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [OTHER CASH OUTFLOWS EXTERNAL EUR],
		ROUND(CAST(REPLACE([Total_Ouflows],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [TOTAL OUTFLOWS EUR],
		ROUND(CAST(REPLACE([Net_cash_flow],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [NET CASH FLOW EUR],
		ROUND(CAST(REPLACE([Closing_Cash_TLF],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [CLOSING CASH TLF EUR],
		ROUND(CAST(REPLACE([Closing_Cash_TLG],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [CLOSING CASH TLG EUR],
		ROUND(CAST(REPLACE([Closing_Cash_Banks],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [CLOSING CASH BANKS EUR],
		ROUND(CAST(REPLACE([Closing_Bank_deposits],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [CLOSING BANK DEPOSITS EUR],
		ROUND(CAST(REPLACE([Closing_Borrowing_TLF],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [CLOSING BORROWING TLF EUR],
		ROUND(CAST(REPLACE([Closing_Borrowing_TLG],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [CLOSING BORROWING TLG EUR],
		ROUND(CAST(REPLACE([Closing_Borrowing_Banks],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [CLOSING BORROWING BANKS EUR],
		ROUND(CAST(REPLACE([Total_Net],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [TOTAL NET EUR],
		ROUND(CAST(REPLACE([Peak_financing],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [PEAK FINANCING EUR],
		ROUND(CAST(REPLACE([Positive_balance_of_cash_pool_members],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [POSITIVE BALANCE CASH POOL MEMBERS],
		ROUND(CAST(REPLACE([Total_Peak],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [TOTAL PEAK EUR],
		ROUND(CAST(REPLACE([Net_cash_flow],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [NET FLOWS EUR],
ROUND(CAST(REPLACE([Total_Inflows],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) + ABS(ROUND(CAST(REPLACE([Total_Ouflows],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0)) AS [GROSS FLOWS EUR],
		ROUND(CAST(REPLACE([Net_cash_flow],',','') as bigint) + CAST(REPLACE([Dividend_paid___capital_reduction],',','') as bigint) - CAST(REPLACE([Dividend_collected___Capital_increase] ,',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [NET W/O DIVIDEND FLOWS EUR],
        ROUND(CAST(REPLACE([Total_Inflows],',','') as bigint) - CAST(REPLACE([Dividend_collected___Capital_increase],',','') as bigint)/CAST([FX_Rates] AS FLOAT) + ABS(CAST(REPLACE([Total_Ouflows],',','') as bigint) + CAST(REPLACE([Dividend_paid___capital_reduction],',','') as bigint))/CAST([FX_Rates] AS FLOAT),0) AS [GROSS W/O DIVIDEND FLOWS EUR]

		FROM [Tetra_AnalyticsDB].[dbo].[CF_2019_04] CF_PERIOD

UNION ALL

SELECT 
    	
CF_PERIOD.[FAS_Code] AS [FAS CODE],
        SUBSTRING(Package_period,4,4) + ' ' + SUBSTRING(Package_period,9,2) AS [FORECAST VERSION],
		SUBSTRING(Package_period,4,4) AS [FORECAST VERSION YEAR],
		SUBSTRING(Package_period,9,2) AS [FORECAST VERSION PERIOD],
		CASE SUBSTRING(Package_period,9,2) WHEN '01' THEN 1 WHEN '02' THEN 1 WHEN '03' THEN 1 
											WHEN '04' THEN 2 WHEN '05' THEN 2 WHEN '06' THEN 2 
											WHEN '07' THEN 3 WHEN '08' THEN 3 WHEN '09' THEN 3 
											WHEN '10' THEN 4 WHEN '11' THEN 4 WHEN '12' THEN 4 
											END [FORECAST VERSION QUARTER],
		CASE WHEN SUBSTRING(Package_period,4,4) = SUBSTRING(Forecast_period,4,4) AND SUBSTRING(Forecast_period,9,2) < SUBSTRING(Package_period,9,2) THEN 'ACTUAL' ELSE 'FORECAST' END [FORECAST TYPE],
		CASE WHEN SUBSTRING(Package_period,4,4) = SUBSTRING(Forecast_period,4,4) AND SUBSTRING(Forecast_period,9,2) < SUBSTRING(Package_period,9,2) THEN 1 ELSE 2 END [FORECAST TYPE NUM],
        SUBSTRING(Forecast_period,4,4)  AS [FORECAST YEAR],
		SUBSTRING(Forecast_period,9,2)  AS [FORECAST PERIOD],
		CASE SUBSTRING(Forecast_period,9,2) WHEN '01' THEN 1 WHEN '02' THEN 1 WHEN '03' THEN 1 
											WHEN '04' THEN 2 WHEN '05' THEN 2 WHEN '06' THEN 2 
											WHEN '07' THEN 3 WHEN '08' THEN 3 WHEN '09' THEN 3 
											WHEN '10' THEN 4 WHEN '11' THEN 4 WHEN '12' THEN 4 
											END [FORECAST QUARTER],
		ROUND(CAST(REPLACE([Opening_Cash_TLF],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [OPENING CASH TLF EUR],
		ROUND(CAST(REPLACE([Opening_Cash_TLG],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [OPENING CASH TLG EUR],
		ROUND(CAST(REPLACE([Opening_Cash_Banks],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [OPENING CASH BANKS EUR],
		ROUND(CAST(REPLACE([Opening_Bank_deposits],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [OPENING BANK DEPOSITS EUR],
		ROUND(CAST(REPLACE([Opening_Borrowing_TLF],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [OPENING BORRROWING TLF EUR],
		ROUND(CAST(REPLACE([Opening_Borrowing_TLG],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [OPENING BORRROWING TLG EUR],
		ROUND(CAST(REPLACE([Opening_Borrowing_Banks],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [OPENING OPENING BORRROWING BANKS EUR],
		ROUND(CAST(REPLACE([Opening_NFP],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [OPENING NFP EUR],
        ROUND(CAST(REPLACE([Collections_inside_Netting],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [COLLECTIONS INSIDE NETTING EUR],
		ROUND(CAST(REPLACE([Collections_outside_Netting],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [COLLECTIONS OUTSIDE NETTING EUR],
		ROUND(CAST(REPLACE([Collections___External_trade_receivables],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [COLLECTIONS EXTERNAL TRADE RECEIVABLES EUR],
		ROUND(CAST(REPLACE([Sales_of_capital_equipment],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [SALES OF CAPITAL EQUIPMENT EUR],
		ROUND(CAST(REPLACE([External_divestments],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [EXTERNAL DIVESTMENTS EUR],
		ROUND(CAST(REPLACE([Income_taxes_refunds],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [INCOME TAXES REFUNDS EUR],
		ROUND(CAST(REPLACE([VAT_refunds],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [VAT REFUNDS EUR],
		ROUND(CAST(REPLACE([Other_taxes_and_duties_refunds],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [OTHER TAXES AND DUTIES REFUNDS EUR],
		ROUND(CAST(REPLACE([Dividend_collected___Capital_increase],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [DIVIDEND COLLECTED CAPITAL INCREASE EUR],
		ROUND(CAST(REPLACE([Royalties_collected],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [ROYALTIES COLLECTED EUR],
		ROUND(CAST(REPLACE([Financial_inflows___Internal],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [FINANCIAL INFLOWS INTERNAL EUR],
		ROUND(CAST(REPLACE([Financial_inflows___External],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [FINANCIAL INFLOWS EXTERNAL EUR],
		ROUND(CAST(REPLACE([Other_cash_inflows___Internal],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [OTHER CASH INFLOWS INTERNAL EUR],
		ROUND(CAST(REPLACE([Other_cash_inflows___External],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [OTHER CASH INFLOWS EXTERNAL EUR],
		ROUND(CAST(REPLACE([Total_Inflows],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [TOTAL INFLOWS EUR],
		ROUND(CAST(REPLACE([Payments_inside_Netting],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [PAYMENTS INSIDE NETTING EUR],
		ROUND(CAST(REPLACE([Payments_outside_Netting],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [PAYMENTS OUTSIDE NETTING EUR],
		ROUND(CAST(REPLACE([Payments___External_trade__payables],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [PAYMENTS EXTERNAL TRADE PAYBABLES],
		ROUND(CAST(REPLACE([_Discounts__rebates__market_support_paid_],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [DISCOUNTS REBATS MARKET SUPPORT PAID EUR],
		ROUND(CAST(REPLACE([External_Investments],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [EXTERNAL INVESTMENTS EUR],
		ROUND(CAST(REPLACE([Income_taxes_paid],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [INCOME TAXES PAID EUR],
		ROUND(CAST(REPLACE([VAT_paid],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [VAT PAID EUR],
		ROUND(CAST(REPLACE([Other_taxes_and_duties_paid],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [OTHER TAXES AND DUTIES PAID EUR],
		ROUND(CAST(REPLACE([Dividend_paid___capital_reduction],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [DIVIDEND PAID CAPITAL REDUCTION EUR],
		ROUND(CAST(REPLACE([Royalties_paid],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [ROYALTIES PAID EUR],
        ROUND(CAST(REPLACE([_Salaries__social_securities_and_pensions_],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [SALARIES SOCIAL SECURITIES AND PENSIONS EUR],
		ROUND(CAST(REPLACE([Financial_outflows___external],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [FINANCIAL OUTFLOWS INTERNAL EUR],
		ROUND(CAST(REPLACE([Financial_outflows___Internal],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [FINANCIAL OUTFLOWS EXTERNAL EUR],	
		ROUND(CAST(REPLACE([Other_cash_outflows___Internal],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [OTHER CASH OUTFLOWS INTERNAL EUR],
		ROUND(CAST(REPLACE([Other_cash_outflows___External],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [OTHER CASH OUTFLOWS EXTERNAL EUR],
		ROUND(CAST(REPLACE([Total_Ouflows],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [TOTAL OUTFLOWS EUR],
		ROUND(CAST(REPLACE([Net_cash_flow],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [NET CASH FLOW EUR],
		ROUND(CAST(REPLACE([Closing_Cash_TLF],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [CLOSING CASH TLF EUR],
		ROUND(CAST(REPLACE([Closing_Cash_TLG],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [CLOSING CASH TLG EUR],
		ROUND(CAST(REPLACE([Closing_Cash_Banks],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [CLOSING CASH BANKS EUR],
		ROUND(CAST(REPLACE([Closing_Bank_deposits],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [CLOSING BANK DEPOSITS EUR],
		ROUND(CAST(REPLACE([Closing_Borrowing_TLF],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [CLOSING BORROWING TLF EUR],
		ROUND(CAST(REPLACE([Closing_Borrowing_TLG],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [CLOSING BORROWING TLG EUR],
		ROUND(CAST(REPLACE([Closing_Borrowing_Banks],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [CLOSING BORROWING BANKS EUR],
		ROUND(CAST(REPLACE([Total_Net],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [TOTAL NET EUR],
		ROUND(CAST(REPLACE([Peak_financing],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [PEAK FINANCING EUR],
		ROUND(CAST(REPLACE([Positive_balance_of_cash_pool_members],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [POSITIVE BALANCE CASH POOL MEMBERS],
		ROUND(CAST(REPLACE([Total_Peak],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [TOTAL PEAK EUR],
		ROUND(CAST(REPLACE([Net_cash_flow],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [NET FLOWS EUR],
        ROUND(CAST(REPLACE([Total_Inflows],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) + ABS(ROUND(CAST(REPLACE([Total_Ouflows],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0)) AS [GROSS FLOWS EUR],
		ROUND(CAST(REPLACE([Net_cash_flow],',','') as bigint) + CAST(REPLACE([Dividend_paid___capital_reduction],',','') as bigint) - CAST(REPLACE([Dividend_collected___Capital_increase] ,',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [NET W/O DIVIDEND FLOWS EUR],
        ROUND(CAST(REPLACE([Total_Inflows],',','') as bigint) - CAST(REPLACE([Dividend_collected___Capital_increase],',','') as bigint)/CAST([FX_Rates] AS FLOAT) + ABS(CAST(REPLACE([Total_Ouflows],',','') as bigint) + CAST(REPLACE([Dividend_paid___capital_reduction],',','') as bigint))/CAST([FX_Rates] AS FLOAT),0) AS [GROSS W/O DIVIDEND FLOWS EUR]
        
FROM [Tetra_AnalyticsDB].[dbo].[CF_2019_05] CF_PERIOD
	
UNION ALL
SELECT 
CF_PERIOD.[FAS_Code] AS [FAS CODE],
        SUBSTRING(Package_period,4,4) + ' ' + SUBSTRING(Package_period,9,2) AS [FORECAST VERSION],
		SUBSTRING(Package_period,4,4) AS [FORECAST VERSION YEAR],
		SUBSTRING(Package_period,9,2) AS [FORECAST VERSION PERIOD],
		CASE SUBSTRING(Package_period,9,2) WHEN '01' THEN 1 WHEN '02' THEN 1 WHEN '03' THEN 1 
											WHEN '04' THEN 2 WHEN '05' THEN 2 WHEN '06' THEN 2 
											WHEN '07' THEN 3 WHEN '08' THEN 3 WHEN '09' THEN 3 
											WHEN '10' THEN 4 WHEN '11' THEN 4 WHEN '12' THEN 4 
											END [FORECAST VERSION QUARTER],
		CASE WHEN SUBSTRING(Package_period,4,4) = SUBSTRING(Forecast_period,4,4) AND SUBSTRING(Forecast_period,9,2) < SUBSTRING(Package_period,9,2) THEN 'ACTUAL' ELSE 'FORECAST' END [FORECAST TYPE],
		CASE WHEN SUBSTRING(Package_period,4,4) = SUBSTRING(Forecast_period,4,4) AND SUBSTRING(Forecast_period,9,2) < SUBSTRING(Package_period,9,2) THEN 1 ELSE 2 END [FORECAST TYPE NUM],
        SUBSTRING(Forecast_period,4,4)  AS [FORECAST YEAR],
		SUBSTRING(Forecast_period,9,2)  AS [FORECAST PERIOD],
		CASE SUBSTRING(Forecast_period,9,2) WHEN '01' THEN 1 WHEN '02' THEN 1 WHEN '03' THEN 1 
											WHEN '04' THEN 2 WHEN '05' THEN 2 WHEN '06' THEN 2 
											WHEN '07' THEN 3 WHEN '08' THEN 3 WHEN '09' THEN 3 
											WHEN '10' THEN 4 WHEN '11' THEN 4 WHEN '12' THEN 4 
											END [FORECAST QUARTER],
		ROUND(CAST(REPLACE([Opening_Cash_TLF],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [OPENING CASH TLF EUR],
		ROUND(CAST(REPLACE([Opening_Cash_TLG],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [OPENING CASH TLG EUR],
		ROUND(CAST(REPLACE([Opening_Cash_Banks],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [OPENING CASH BANKS EUR],
		ROUND(CAST(REPLACE([Opening_Bank_deposits],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [OPENING BANK DEPOSITS EUR],
		ROUND(CAST(REPLACE([Opening_Borrowing_TLF],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [OPENING BORRROWING TLF EUR],
		ROUND(CAST(REPLACE([Opening_Borrowing_TLG],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [OPENING BORRROWING TLG EUR],
		ROUND(CAST(REPLACE([Opening_Borrowing_Banks],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [OPENING OPENING BORRROWING BANKS EUR],
		ROUND(CAST(REPLACE([Opening_NFP],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [OPENING NFP EUR],
        ROUND(CAST(REPLACE([Collections_inside_Netting],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [COLLECTIONS INSIDE NETTING EUR],
		ROUND(CAST(REPLACE([Collections_outside_Netting],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [COLLECTIONS OUTSIDE NETTING EUR],
		ROUND(CAST(REPLACE([Collections___External_trade_receivables],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [COLLECTIONS EXTERNAL TRADE RECEIVABLES EUR],
		ROUND(CAST(REPLACE([Sales_of_capital_equipment],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [SALES OF CAPITAL EQUIPMENT EUR],
		ROUND(CAST(REPLACE([External_divestments],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [EXTERNAL DIVESTMENTS EUR],
		ROUND(CAST(REPLACE([Income_taxes_refunds],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [INCOME TAXES REFUNDS EUR],
		ROUND(CAST(REPLACE([VAT_refunds],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [VAT REFUNDS EUR],
		ROUND(CAST(REPLACE([Other_taxes_and_duties_refunds],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [OTHER TAXES AND DUTIES REFUNDS EUR],
		ROUND(CAST(REPLACE([Dividend_collected___Capital_increase],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [DIVIDEND COLLECTED CAPITAL INCREASE EUR],
		ROUND(CAST(REPLACE([Royalties_collected],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [ROYALTIES COLLECTED EUR],
		ROUND(CAST(REPLACE([Financial_inflows___Internal],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [FINANCIAL INFLOWS INTERNAL EUR],
		ROUND(CAST(REPLACE([Financial_inflows___External],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [FINANCIAL INFLOWS EXTERNAL EUR],
		ROUND(CAST(REPLACE([Other_cash_inflows___Internal],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [OTHER CASH INFLOWS INTERNAL EUR],
		ROUND(CAST(REPLACE([Other_cash_inflows___External],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [OTHER CASH INFLOWS EXTERNAL EUR],
		ROUND(CAST(REPLACE([Total_Inflows],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [TOTAL INFLOWS EUR],
		ROUND(CAST(REPLACE([Payments_inside_Netting],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [PAYMENTS INSIDE NETTING EUR],
		ROUND(CAST(REPLACE([Payments_outside_Netting],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [PAYMENTS OUTSIDE NETTING EUR],
		ROUND(CAST(REPLACE([Payments___External_trade__payables],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [PAYMENTS EXTERNAL TRADE PAYBABLES],
		ROUND(CAST(REPLACE([_Discounts__rebates__market_support_paid_],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [DISCOUNTS REBATS MARKET SUPPORT PAID EUR],
		ROUND(CAST(REPLACE([External_Investments],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [EXTERNAL INVESTMENTS EUR],
		ROUND(CAST(REPLACE([Income_taxes_paid],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [INCOME TAXES PAID EUR],
		ROUND(CAST(REPLACE([VAT_paid],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [VAT PAID EUR],
		ROUND(CAST(REPLACE([Other_taxes_and_duties_paid],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [OTHER TAXES AND DUTIES PAID EUR],
		ROUND(CAST(REPLACE([Dividend_paid___capital_reduction],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [DIVIDEND PAID CAPITAL REDUCTION EUR],
		ROUND(CAST(REPLACE([Royalties_paid],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [ROYALTIES PAID EUR],
        ROUND(CAST(REPLACE([_Salaries__social_securities_and_pensions_],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [SALARIES SOCIAL SECURITIES AND PENSIONS EUR],
		ROUND(CAST(REPLACE([Financial_outflows___external],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [FINANCIAL OUTFLOWS INTERNAL EUR],
		ROUND(CAST(REPLACE([Financial_outflows___Internal],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [FINANCIAL OUTFLOWS EXTERNAL EUR],	
		ROUND(CAST(REPLACE([Other_cash_outflows___Internal],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [OTHER CASH OUTFLOWS INTERNAL EUR],
		ROUND(CAST(REPLACE([Other_cash_outflows___External],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [OTHER CASH OUTFLOWS EXTERNAL EUR],
		ROUND(CAST(REPLACE([Total_Ouflows],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [TOTAL OUTFLOWS EUR],
		ROUND(CAST(REPLACE([Net_cash_flow],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [NET CASH FLOW EUR],
		ROUND(CAST(REPLACE([Closing_Cash_TLF],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [CLOSING CASH TLF EUR],
		ROUND(CAST(REPLACE([Closing_Cash_TLG],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [CLOSING CASH TLG EUR],
		ROUND(CAST(REPLACE([Closing_Cash_Banks],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [CLOSING CASH BANKS EUR],
		ROUND(CAST(REPLACE([Closing_Bank_deposits],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [CLOSING BANK DEPOSITS EUR],
		ROUND(CAST(REPLACE([Closing_Borrowing_TLF],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [CLOSING BORROWING TLF EUR],
		ROUND(CAST(REPLACE([Closing_Borrowing_TLG],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [CLOSING BORROWING TLG EUR],
		ROUND(CAST(REPLACE([Closing_Borrowing_Banks],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [CLOSING BORROWING BANKS EUR],
		ROUND(CAST(REPLACE([Total_Net],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [TOTAL NET EUR],
		ROUND(CAST(REPLACE([Peak_financing],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [PEAK FINANCING EUR],
		ROUND(CAST(REPLACE([Positive_balance_of_cash_pool_members],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [POSITIVE BALANCE CASH POOL MEMBERS],
		ROUND(CAST(REPLACE([Total_Peak],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [TOTAL PEAK EUR],
		ROUND(CAST(REPLACE([Net_cash_flow],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [NET FLOWS EUR],
	    ROUND(CAST(REPLACE([Total_Inflows],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) + ABS(ROUND(CAST(REPLACE([Total_Ouflows],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0)) AS [GROSS FLOWS EUR],
		ROUND(CAST(REPLACE([Net_cash_flow],',','') as bigint) + CAST(REPLACE([Dividend_paid___capital_reduction],',','') as bigint) - CAST(REPLACE([Dividend_collected___Capital_increase] ,',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [NET W/O DIVIDEND FLOWS EUR],
        ROUND(CAST(REPLACE([Total_Inflows],',','') as bigint) - CAST(REPLACE([Dividend_collected___Capital_increase],',','') as bigint)/CAST([FX_Rates] AS FLOAT) + ABS(CAST(REPLACE([Total_Ouflows],',','') as bigint) + CAST(REPLACE([Dividend_paid___capital_reduction],',','') as bigint))/CAST([FX_Rates] AS FLOAT),0) AS [GROSS W/O DIVIDEND FLOWS EUR]
        

FROM [Tetra_AnalyticsDB].[dbo].[CF_2019_06] CF_PERIOD
		
UNION ALL

SELECT  
    
CF_PERIOD.[FAS_Code] AS [FAS CODE],
        SUBSTRING(Package_period,4,4) + ' ' + SUBSTRING(Package_period,9,2) AS [FORECAST VERSION],
		SUBSTRING(Package_period,4,4) AS [FORECAST VERSION YEAR],
		SUBSTRING(Package_period,9,2) AS [FORECAST VERSION PERIOD],
		CASE SUBSTRING(Package_period,9,2) WHEN '01' THEN 1 WHEN '02' THEN 1 WHEN '03' THEN 1 
											WHEN '04' THEN 2 WHEN '05' THEN 2 WHEN '06' THEN 2 
											WHEN '07' THEN 3 WHEN '08' THEN 3 WHEN '09' THEN 3 
											WHEN '10' THEN 4 WHEN '11' THEN 4 WHEN '12' THEN 4 
											END [FORECAST VERSION QUARTER],
		CASE WHEN SUBSTRING(Package_period,4,4) = SUBSTRING(Forecast_period,4,4) AND SUBSTRING(Forecast_period,9,2) < SUBSTRING(Package_period,9,2) THEN 'ACTUAL' ELSE 'FORECAST' END [FORECAST TYPE],
		CASE WHEN SUBSTRING(Package_period,4,4) = SUBSTRING(Forecast_period,4,4) AND SUBSTRING(Forecast_period,9,2) < SUBSTRING(Package_period,9,2) THEN 1 ELSE 2 END [FORECAST TYPE NUM],
        SUBSTRING(Forecast_period,4,4)  AS [FORECAST YEAR],
		SUBSTRING(Forecast_period,9,2)  AS [FORECAST PERIOD],
		CASE SUBSTRING(Forecast_period,9,2) WHEN '01' THEN 1 WHEN '02' THEN 1 WHEN '03' THEN 1 
											WHEN '04' THEN 2 WHEN '05' THEN 2 WHEN '06' THEN 2 
											WHEN '07' THEN 3 WHEN '08' THEN 3 WHEN '09' THEN 3 
											WHEN '10' THEN 4 WHEN '11' THEN 4 WHEN '12' THEN 4 
											END [FORECAST QUARTER],
		ROUND(CAST(REPLACE([Opening_Cash_TLF],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [OPENING CASH TLF EUR],
		ROUND(CAST(REPLACE([Opening_Cash_TLG],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [OPENING CASH TLG EUR],
		ROUND(CAST(REPLACE([Opening_Cash_Banks],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [OPENING CASH BANKS EUR],
		ROUND(CAST(REPLACE([Opening_Bank_deposits],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [OPENING BANK DEPOSITS EUR],
		ROUND(CAST(REPLACE([Opening_Borrowing_TLF],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [OPENING BORRROWING TLF EUR],
		ROUND(CAST(REPLACE([Opening_Borrowing_TLG],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [OPENING BORRROWING TLG EUR],
		ROUND(CAST(REPLACE([Opening_Borrowing_Banks],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [OPENING OPENING BORRROWING BANKS EUR],
		ROUND(CAST(REPLACE([Opening_NFP],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [OPENING NFP EUR],
        ROUND(CAST(REPLACE([Collections_inside_Netting],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [COLLECTIONS INSIDE NETTING EUR],
		ROUND(CAST(REPLACE([Collections_outside_Netting],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [COLLECTIONS OUTSIDE NETTING EUR],
		ROUND(CAST(REPLACE([Collections___External_trade_receivables],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [COLLECTIONS EXTERNAL TRADE RECEIVABLES EUR],
		ROUND(CAST(REPLACE([Sales_of_capital_equipment],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [SALES OF CAPITAL EQUIPMENT EUR],
		ROUND(CAST(REPLACE([External_divestments],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [EXTERNAL DIVESTMENTS EUR],
		ROUND(CAST(REPLACE([Income_taxes_refunds],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [INCOME TAXES REFUNDS EUR],
		ROUND(CAST(REPLACE([VAT_refunds],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [VAT REFUNDS EUR],
		ROUND(CAST(REPLACE([Other_taxes_and_duties_refunds],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [OTHER TAXES AND DUTIES REFUNDS EUR],
		ROUND(CAST(REPLACE([Dividend_collected___Capital_increase],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [DIVIDEND COLLECTED CAPITAL INCREASE EUR],
		ROUND(CAST(REPLACE([Royalties_collected],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [ROYALTIES COLLECTED EUR],
		ROUND(CAST(REPLACE([Financial_inflows___Internal],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [FINANCIAL INFLOWS INTERNAL EUR],
		ROUND(CAST(REPLACE([Financial_inflows___External],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [FINANCIAL INFLOWS EXTERNAL EUR],
		ROUND(CAST(REPLACE([Other_cash_inflows___Internal],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [OTHER CASH INFLOWS INTERNAL EUR],
		ROUND(CAST(REPLACE([Other_cash_inflows___External],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [OTHER CASH INFLOWS EXTERNAL EUR],
		ROUND(CAST(REPLACE([Total_Inflows],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [TOTAL INFLOWS EUR],
		ROUND(CAST(REPLACE([Payments_inside_Netting],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [PAYMENTS INSIDE NETTING EUR],
		ROUND(CAST(REPLACE([Payments_outside_Netting],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [PAYMENTS OUTSIDE NETTING EUR],
		ROUND(CAST(REPLACE([Payments___External_trade__payables],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [PAYMENTS EXTERNAL TRADE PAYBABLES],
		ROUND(CAST(REPLACE([_Discounts__rebates__market_support_paid_],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [DISCOUNTS REBATS MARKET SUPPORT PAID EUR],
		ROUND(CAST(REPLACE([External_Investments],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [EXTERNAL INVESTMENTS EUR],
		ROUND(CAST(REPLACE([Income_taxes_paid],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [INCOME TAXES PAID EUR],
		ROUND(CAST(REPLACE([VAT_paid],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [VAT PAID EUR],
		ROUND(CAST(REPLACE([Other_taxes_and_duties_paid],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [OTHER TAXES AND DUTIES PAID EUR],
		ROUND(CAST(REPLACE([Dividend_paid___capital_reduction],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [DIVIDEND PAID CAPITAL REDUCTION EUR],
		ROUND(CAST(REPLACE([Royalties_paid],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [ROYALTIES PAID EUR],
        ROUND(CAST(REPLACE([_Salaries__social_securities_and_pensions_],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [SALARIES SOCIAL SECURITIES AND PENSIONS EUR],
		ROUND(CAST(REPLACE([Financial_outflows___external],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [FINANCIAL OUTFLOWS INTERNAL EUR],
		ROUND(CAST(REPLACE([Financial_outflows___Internal],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [FINANCIAL OUTFLOWS EXTERNAL EUR],	
		ROUND(CAST(REPLACE([Other_cash_outflows___Internal],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [OTHER CASH OUTFLOWS INTERNAL EUR],
		ROUND(CAST(REPLACE([Other_cash_outflows___External],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [OTHER CASH OUTFLOWS EXTERNAL EUR],
		ROUND(CAST(REPLACE([Total_Ouflows],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [TOTAL OUTFLOWS EUR],
		ROUND(CAST(REPLACE([Net_cash_flow],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [NET CASH FLOW EUR],
		ROUND(CAST(REPLACE([Closing_Cash_TLF],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [CLOSING CASH TLF EUR],
		ROUND(CAST(REPLACE([Closing_Cash_TLG],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [CLOSING CASH TLG EUR],
		ROUND(CAST(REPLACE([Closing_Cash_Banks],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [CLOSING CASH BANKS EUR],
		ROUND(CAST(REPLACE([Closing_Bank_deposits],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [CLOSING BANK DEPOSITS EUR],
		ROUND(CAST(REPLACE([Closing_Borrowing_TLF],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [CLOSING BORROWING TLF EUR],
		ROUND(CAST(REPLACE([Closing_Borrowing_TLG],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [CLOSING BORROWING TLG EUR],
		ROUND(CAST(REPLACE([Closing_Borrowing_Banks],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [CLOSING BORROWING BANKS EUR],
		ROUND(CAST(REPLACE([Total_Net],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [TOTAL NET EUR],
		ROUND(CAST(REPLACE([Peak_financing],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [PEAK FINANCING EUR],
		ROUND(CAST(REPLACE([Positive_balance_of_cash_pool_members],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [POSITIVE BALANCE CASH POOL MEMBERS],
		ROUND(CAST(REPLACE([Total_Peak],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [TOTAL PEAK EUR],
		ROUND(CAST(REPLACE([Net_cash_flow],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [NET FLOWS EUR],
		ROUND(CAST(REPLACE([Total_Inflows],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) + ABS(ROUND(CAST(REPLACE([Total_Ouflows],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0)) AS [GROSS FLOWS EUR],
		ROUND(CAST(REPLACE([Net_cash_flow],',','') as bigint) + CAST(REPLACE([Dividend_paid___capital_reduction],',','') as bigint) - CAST(REPLACE([Dividend_collected___Capital_increase] ,',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [NET W/O DIVIDEND FLOWS EUR],
        ROUND(CAST(REPLACE([Total_Inflows],',','') as bigint) - CAST(REPLACE([Dividend_collected___Capital_increase],',','') as bigint)/CAST([FX_Rates] AS FLOAT) + ABS(CAST(REPLACE([Total_Ouflows],',','') as bigint) + CAST(REPLACE([Dividend_paid___capital_reduction],',','') as bigint))/CAST([FX_Rates] AS FLOAT),0) AS [GROSS W/O DIVIDEND FLOWS EUR]
        
        
FROM [Tetra_AnalyticsDB].[dbo].[CF_2019_07] CF_PERIOD
		
UNION ALL

SELECT 
  
        CF_PERIOD.[FAS_Code] AS [FAS CODE],
        SUBSTRING(Package_period,4,4) + ' ' + SUBSTRING(Package_period,9,2) AS [FORECAST VERSION],
		SUBSTRING(Package_period,4,4) AS [FORECAST VERSION YEAR],
		SUBSTRING(Package_period,9,2) AS [FORECAST VERSION PERIOD],
		CASE SUBSTRING(Package_period,9,2) WHEN '01' THEN 1 WHEN '02' THEN 1 WHEN '03' THEN 1 
											WHEN '04' THEN 2 WHEN '05' THEN 2 WHEN '06' THEN 2 
											WHEN '07' THEN 3 WHEN '08' THEN 3 WHEN '09' THEN 3 
											WHEN '10' THEN 4 WHEN '11' THEN 4 WHEN '12' THEN 4 
											END [FORECAST VERSION QUARTER],
		CASE WHEN SUBSTRING(Package_period,4,4) = SUBSTRING(Forecast_period,4,4) AND SUBSTRING(Forecast_period,9,2) < SUBSTRING(Package_period,9,2) THEN 'ACTUAL' ELSE 'FORECAST' END [FORECAST TYPE],
		CASE WHEN SUBSTRING(Package_period,4,4) = SUBSTRING(Forecast_period,4,4) AND SUBSTRING(Forecast_period,9,2) < SUBSTRING(Package_period,9,2) THEN 1 ELSE 2 END [FORECAST TYPE NUM],
        SUBSTRING(Forecast_period,4,4)  AS [FORECAST YEAR],
		SUBSTRING(Forecast_period,9,2)  AS [FORECAST PERIOD],
		CASE SUBSTRING(Forecast_period,9,2) WHEN '01' THEN 1 WHEN '02' THEN 1 WHEN '03' THEN 1 
											WHEN '04' THEN 2 WHEN '05' THEN 2 WHEN '06' THEN 2 
											WHEN '07' THEN 3 WHEN '08' THEN 3 WHEN '09' THEN 3 
											WHEN '10' THEN 4 WHEN '11' THEN 4 WHEN '12' THEN 4 
											END [FORECAST QUARTER],
		ROUND(CAST(REPLACE([Opening_Cash_TLF],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [OPENING CASH TLF EUR],
		ROUND(CAST(REPLACE([Opening_Cash_TLG],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [OPENING CASH TLG EUR],
		ROUND(CAST(REPLACE([Opening_Cash_Banks],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [OPENING CASH BANKS EUR],
		ROUND(CAST(REPLACE([Opening_Bank_deposits],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [OPENING BANK DEPOSITS EUR],
		ROUND(CAST(REPLACE([Opening_Borrowing_TLF],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [OPENING BORRROWING TLF EUR],
		ROUND(CAST(REPLACE([Opening_Borrowing_TLG],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [OPENING BORRROWING TLG EUR],
		ROUND(CAST(REPLACE([Opening_Borrowing_Banks],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [OPENING OPENING BORRROWING BANKS EUR],
		ROUND(CAST(REPLACE([Opening_NFP],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [OPENING NFP EUR],
        ROUND(CAST(REPLACE([Collections_inside_Netting],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [COLLECTIONS INSIDE NETTING EUR],
		ROUND(CAST(REPLACE([Collections_outside_Netting],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [COLLECTIONS OUTSIDE NETTING EUR],
		ROUND(CAST(REPLACE([Collections___External_trade_receivables],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [COLLECTIONS EXTERNAL TRADE RECEIVABLES EUR],
		ROUND(CAST(REPLACE([Sales_of_capital_equipment],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [SALES OF CAPITAL EQUIPMENT EUR],
		ROUND(CAST(REPLACE([External_divestments],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [EXTERNAL DIVESTMENTS EUR],
		ROUND(CAST(REPLACE([Income_taxes_refunds],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [INCOME TAXES REFUNDS EUR],
		ROUND(CAST(REPLACE([VAT_refunds],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [VAT REFUNDS EUR],
		ROUND(CAST(REPLACE([Other_taxes_and_duties_refunds],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [OTHER TAXES AND DUTIES REFUNDS EUR],
		ROUND(CAST(REPLACE([Dividend_collected___Capital_increase],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [DIVIDEND COLLECTED CAPITAL INCREASE EUR],
		ROUND(CAST(REPLACE([Royalties_collected],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [ROYALTIES COLLECTED EUR],
		ROUND(CAST(REPLACE([Financial_inflows___Internal],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [FINANCIAL INFLOWS INTERNAL EUR],
		ROUND(CAST(REPLACE([Financial_inflows___External],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [FINANCIAL INFLOWS EXTERNAL EUR],
		ROUND(CAST(REPLACE([Other_cash_inflows___Internal],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [OTHER CASH INFLOWS INTERNAL EUR],
		ROUND(CAST(REPLACE([Other_cash_inflows___External],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [OTHER CASH INFLOWS EXTERNAL EUR],
		ROUND(CAST(REPLACE([Total_Inflows],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [TOTAL INFLOWS EUR],
		ROUND(CAST(REPLACE([Payments_inside_Netting],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [PAYMENTS INSIDE NETTING EUR],
		ROUND(CAST(REPLACE([Payments_outside_Netting],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [PAYMENTS OUTSIDE NETTING EUR],
		ROUND(CAST(REPLACE([Payments___External_trade__payables],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [PAYMENTS EXTERNAL TRADE PAYBABLES],
		ROUND(CAST(REPLACE([_Discounts__rebates__market_support_paid_],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [DISCOUNTS REBATS MARKET SUPPORT PAID EUR],
		ROUND(CAST(REPLACE([External_Investments],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [EXTERNAL INVESTMENTS EUR],
		ROUND(CAST(REPLACE([Income_taxes_paid],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [INCOME TAXES PAID EUR],
		ROUND(CAST(REPLACE([VAT_paid],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [VAT PAID EUR],
		ROUND(CAST(REPLACE([Other_taxes_and_duties_paid],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [OTHER TAXES AND DUTIES PAID EUR],
		ROUND(CAST(REPLACE([Dividend_paid___capital_reduction],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [DIVIDEND PAID CAPITAL REDUCTION EUR],
		ROUND(CAST(REPLACE([Royalties_paid],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [ROYALTIES PAID EUR],
        ROUND(CAST(REPLACE([_Salaries__social_securities_and_pensions_],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [SALARIES SOCIAL SECURITIES AND PENSIONS EUR],
		ROUND(CAST(REPLACE([Financial_outflows___external],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [FINANCIAL OUTFLOWS INTERNAL EUR],
		ROUND(CAST(REPLACE([Financial_outflows___Internal],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [FINANCIAL OUTFLOWS EXTERNAL EUR],	
		ROUND(CAST(REPLACE([Other_cash_outflows___Internal],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [OTHER CASH OUTFLOWS INTERNAL EUR],
		ROUND(CAST(REPLACE([Other_cash_outflows___External],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [OTHER CASH OUTFLOWS EXTERNAL EUR],
		ROUND(CAST(REPLACE([Total_Ouflows],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [TOTAL OUTFLOWS EUR],
		ROUND(CAST(REPLACE([Net_cash_flow],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [NET CASH FLOW EUR],
		ROUND(CAST(REPLACE([Closing_Cash_TLF],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [CLOSING CASH TLF EUR],
		ROUND(CAST(REPLACE([Closing_Cash_TLG],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [CLOSING CASH TLG EUR],
		ROUND(CAST(REPLACE([Closing_Cash_Banks],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [CLOSING CASH BANKS EUR],
		ROUND(CAST(REPLACE([Closing_Bank_deposits],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [CLOSING BANK DEPOSITS EUR],
		ROUND(CAST(REPLACE([Closing_Borrowing_TLF],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [CLOSING BORROWING TLF EUR],
		ROUND(CAST(REPLACE([Closing_Borrowing_TLG],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [CLOSING BORROWING TLG EUR],
		ROUND(CAST(REPLACE([Closing_Borrowing_Banks],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [CLOSING BORROWING BANKS EUR],
		ROUND(CAST(REPLACE([Total_Net],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [TOTAL NET EUR],
		ROUND(CAST(REPLACE([Peak_financing],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [PEAK FINANCING EUR],
		ROUND(CAST(REPLACE([Positive_balance_of_cash_pool_members],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [POSITIVE BALANCE CASH POOL MEMBERS],
		ROUND(CAST(REPLACE([Total_Peak],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [TOTAL PEAK EUR],
		ROUND(CAST(REPLACE([Net_cash_flow],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [NET FLOWS EUR],
        ROUND(CAST(REPLACE([Total_Inflows],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) + ABS(ROUND(CAST(REPLACE([Total_Ouflows],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0)) AS [GROSS FLOWS EUR],
		ROUND(CAST(REPLACE([Net_cash_flow],',','') as bigint) + CAST(REPLACE([Dividend_paid___capital_reduction],',','') as bigint) - CAST(REPLACE([Dividend_collected___Capital_increase] ,',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [NET W/O DIVIDEND FLOWS EUR],
        ROUND(CAST(REPLACE([Total_Inflows],',','') as bigint) - CAST(REPLACE([Dividend_collected___Capital_increase],',','') as bigint)/CAST([FX_Rates] AS FLOAT) + ABS(CAST(REPLACE([Total_Ouflows],',','') as bigint) + CAST(REPLACE([Dividend_paid___capital_reduction],',','') as bigint))/CAST([FX_Rates] AS FLOAT),0) AS [GROSS W/O DIVIDEND FLOWS EUR]
        

FROM [Tetra_AnalyticsDB].[dbo].[CF_2019_08] CF_PERIOD
		
UNION ALL

SELECT 

		CF_PERIOD.[FAS_Code] AS [FAS CODE],
        SUBSTRING(Package_period,4,4) + ' ' + SUBSTRING(Package_period,9,2) AS [FORECAST VERSION],
		SUBSTRING(Package_period,4,4) AS [FORECAST VERSION YEAR],
		SUBSTRING(Package_period,9,2) AS [FORECAST VERSION PERIOD],
		CASE SUBSTRING(Package_period,9,2) WHEN '01' THEN 1 WHEN '02' THEN 1 WHEN '03' THEN 1 
											WHEN '04' THEN 2 WHEN '05' THEN 2 WHEN '06' THEN 2 
											WHEN '07' THEN 3 WHEN '08' THEN 3 WHEN '09' THEN 3 
											WHEN '10' THEN 4 WHEN '11' THEN 4 WHEN '12' THEN 4 
											END [FORECAST VERSION QUARTER],
		CASE WHEN SUBSTRING(Package_period,4,4) = SUBSTRING(Forecast_period,4,4) AND SUBSTRING(Forecast_period,9,2) < SUBSTRING(Package_period,9,2) THEN 'ACTUAL' ELSE 'FORECAST' END [FORECAST TYPE],
		CASE WHEN SUBSTRING(Package_period,4,4) = SUBSTRING(Forecast_period,4,4) AND SUBSTRING(Forecast_period,9,2) < SUBSTRING(Package_period,9,2) THEN 1 ELSE 2 END [FORECAST TYPE NUM],
        SUBSTRING(Forecast_period,4,4)  AS [FORECAST YEAR],
		SUBSTRING(Forecast_period,9,2)  AS [FORECAST PERIOD],
		CASE SUBSTRING(Forecast_period,9,2) WHEN '01' THEN 1 WHEN '02' THEN 1 WHEN '03' THEN 1 
											WHEN '04' THEN 2 WHEN '05' THEN 2 WHEN '06' THEN 2 
											WHEN '07' THEN 3 WHEN '08' THEN 3 WHEN '09' THEN 3 
											WHEN '10' THEN 4 WHEN '11' THEN 4 WHEN '12' THEN 4 
											END [FORECAST QUARTER],
		ROUND(CAST(REPLACE([Opening_Cash_TLF],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [OPENING CASH TLF EUR],
		ROUND(CAST(REPLACE([Opening_Cash_TLG],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [OPENING CASH TLG EUR],
		ROUND(CAST(REPLACE([Opening_Cash_Banks],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [OPENING CASH BANKS EUR],
		ROUND(CAST(REPLACE([Opening_Bank_deposits],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [OPENING BANK DEPOSITS EUR],
		ROUND(CAST(REPLACE([Opening_Borrowing_TLF],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [OPENING BORRROWING TLF EUR],
		ROUND(CAST(REPLACE([Opening_Borrowing_TLG],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [OPENING BORRROWING TLG EUR],
		ROUND(CAST(REPLACE([Opening_Borrowing_Banks],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [OPENING OPENING BORRROWING BANKS EUR],
		ROUND(CAST(REPLACE([Opening_NFP],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [OPENING NFP EUR],
        ROUND(CAST(REPLACE([Collections_inside_Netting],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [COLLECTIONS INSIDE NETTING EUR],
		ROUND(CAST(REPLACE([Collections_outside_Netting],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [COLLECTIONS OUTSIDE NETTING EUR],
		ROUND(CAST(REPLACE([Collections___External_trade_receivables],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [COLLECTIONS EXTERNAL TRADE RECEIVABLES EUR],
		ROUND(CAST(REPLACE([Sales_of_capital_equipment],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [SALES OF CAPITAL EQUIPMENT EUR],
		ROUND(CAST(REPLACE([External_divestments],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [EXTERNAL DIVESTMENTS EUR],
		ROUND(CAST(REPLACE([Income_taxes_refunds],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [INCOME TAXES REFUNDS EUR],
		ROUND(CAST(REPLACE([VAT_refunds],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [VAT REFUNDS EUR],
		ROUND(CAST(REPLACE([Other_taxes_and_duties_refunds],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [OTHER TAXES AND DUTIES REFUNDS EUR],
		ROUND(CAST(REPLACE([Dividend_collected___Capital_increase],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [DIVIDEND COLLECTED CAPITAL INCREASE EUR],
		ROUND(CAST(REPLACE([Royalties_collected],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [ROYALTIES COLLECTED EUR],
		ROUND(CAST(REPLACE([Financial_inflows___Internal],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [FINANCIAL INFLOWS INTERNAL EUR],
		ROUND(CAST(REPLACE([Financial_inflows___External],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [FINANCIAL INFLOWS EXTERNAL EUR],
		ROUND(CAST(REPLACE([Other_cash_inflows___Internal],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [OTHER CASH INFLOWS INTERNAL EUR],
		ROUND(CAST(REPLACE([Other_cash_inflows___External],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [OTHER CASH INFLOWS EXTERNAL EUR],
		ROUND(CAST(REPLACE([Total_Inflows],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [TOTAL INFLOWS EUR],
		ROUND(CAST(REPLACE([Payments_inside_Netting],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [PAYMENTS INSIDE NETTING EUR],
		ROUND(CAST(REPLACE([Payments_outside_Netting],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [PAYMENTS OUTSIDE NETTING EUR],
		ROUND(CAST(REPLACE([Payments___External_trade__payables],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [PAYMENTS EXTERNAL TRADE PAYBABLES],
		ROUND(CAST(REPLACE([_Discounts__rebates__market_support_paid_],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [DISCOUNTS REBATS MARKET SUPPORT PAID EUR],
		ROUND(CAST(REPLACE([External_Investments],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [EXTERNAL INVESTMENTS EUR],
		ROUND(CAST(REPLACE([Income_taxes_paid],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [INCOME TAXES PAID EUR],
		ROUND(CAST(REPLACE([VAT_paid],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [VAT PAID EUR],
		ROUND(CAST(REPLACE([Other_taxes_and_duties_paid],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [OTHER TAXES AND DUTIES PAID EUR],
		ROUND(CAST(REPLACE([Dividend_paid___capital_reduction],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [DIVIDEND PAID CAPITAL REDUCTION EUR],
		ROUND(CAST(REPLACE([Royalties_paid],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [ROYALTIES PAID EUR],
        ROUND(CAST(REPLACE([_Salaries__social_securities_and_pensions_],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [SALARIES SOCIAL SECURITIES AND PENSIONS EUR],
		ROUND(CAST(REPLACE([Financial_outflows___external],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [FINANCIAL OUTFLOWS INTERNAL EUR],
		ROUND(CAST(REPLACE([Financial_outflows___Internal],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [FINANCIAL OUTFLOWS EXTERNAL EUR],	
		ROUND(CAST(REPLACE([Other_cash_outflows___Internal],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [OTHER CASH OUTFLOWS INTERNAL EUR],
		ROUND(CAST(REPLACE([Other_cash_outflows___External],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [OTHER CASH OUTFLOWS EXTERNAL EUR],
		ROUND(CAST(REPLACE([Total_Ouflows],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [TOTAL OUTFLOWS EUR],
		ROUND(CAST(REPLACE([Net_cash_flow],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [NET CASH FLOW EUR],
		ROUND(CAST(REPLACE([Closing_Cash_TLF],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [CLOSING CASH TLF EUR],
		ROUND(CAST(REPLACE([Closing_Cash_TLG],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [CLOSING CASH TLG EUR],
		ROUND(CAST(REPLACE([Closing_Cash_Banks],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [CLOSING CASH BANKS EUR],
		ROUND(CAST(REPLACE([Closing_Bank_deposits],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [CLOSING BANK DEPOSITS EUR],
		ROUND(CAST(REPLACE([Closing_Borrowing_TLF],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [CLOSING BORROWING TLF EUR],
		ROUND(CAST(REPLACE([Closing_Borrowing_TLG],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [CLOSING BORROWING TLG EUR],
		ROUND(CAST(REPLACE([Closing_Borrowing_Banks],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [CLOSING BORROWING BANKS EUR],
		ROUND(CAST(REPLACE([Total_Net],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [TOTAL NET EUR],
		ROUND(CAST(REPLACE([Peak_financing],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [PEAK FINANCING EUR],
		ROUND(CAST(REPLACE([Positive_balance_of_cash_pool_members],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [POSITIVE BALANCE CASH POOL MEMBERS],
		ROUND(CAST(REPLACE([Total_Peak],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [TOTAL PEAK EUR],
		ROUND(CAST(REPLACE([Net_cash_flow],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [NET FLOWS EUR],
	ROUND(CAST(REPLACE([Total_Inflows],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) + ABS(ROUND(CAST(REPLACE([Total_Ouflows],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0)) AS [GROSS FLOWS EUR],
		ROUND(CAST(REPLACE([Net_cash_flow],',','') as bigint) + CAST(REPLACE([Dividend_paid___capital_reduction],',','') as bigint) - CAST(REPLACE([Dividend_collected___Capital_increase] ,',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [NET W/O DIVIDEND FLOWS EUR],
        ROUND(CAST(REPLACE([Total_Inflows],',','') as bigint) - CAST(REPLACE([Dividend_collected___Capital_increase],',','') as bigint)/CAST([FX_Rates] AS FLOAT) + ABS(CAST(REPLACE([Total_Ouflows],',','') as bigint) + CAST(REPLACE([Dividend_paid___capital_reduction],',','') as bigint))/CAST([FX_Rates] AS FLOAT),0) AS [GROSS W/O DIVIDEND FLOWS EUR]
 
FROM [Tetra_AnalyticsDB].[dbo].[CF_2019_09] CF_PERIOD

UNION ALL
SELECT 

		CF_PERIOD.[FAS_Code] AS [FAS CODE],
        SUBSTRING(Package_period,4,4) + ' ' + SUBSTRING(Package_period,9,2) AS [FORECAST VERSION],
		SUBSTRING(Package_period,4,4) AS [FORECAST VERSION YEAR],
		SUBSTRING(Package_period,9,2) AS [FORECAST VERSION PERIOD],
		CASE SUBSTRING(Package_period,9,2) WHEN '01' THEN 1 WHEN '02' THEN 1 WHEN '03' THEN 1 
											WHEN '04' THEN 2 WHEN '05' THEN 2 WHEN '06' THEN 2 
											WHEN '07' THEN 3 WHEN '08' THEN 3 WHEN '09' THEN 3 
											WHEN '10' THEN 4 WHEN '11' THEN 4 WHEN '12' THEN 4 
											END [FORECAST VERSION QUARTER],
		CASE WHEN SUBSTRING(Package_period,4,4) = SUBSTRING(Forecast_period,4,4) AND SUBSTRING(Forecast_period,9,2) < SUBSTRING(Package_period,9,2) THEN 'ACTUAL' ELSE 'FORECAST' END [FORECAST TYPE],
		CASE WHEN SUBSTRING(Package_period,4,4) = SUBSTRING(Forecast_period,4,4) AND SUBSTRING(Forecast_period,9,2) < SUBSTRING(Package_period,9,2) THEN 1 ELSE 2 END [FORECAST TYPE NUM],
        SUBSTRING(Forecast_period,4,4)  AS [FORECAST YEAR],
		SUBSTRING(Forecast_period,9,2)  AS [FORECAST PERIOD],
		CASE SUBSTRING(Forecast_period,9,2) WHEN '01' THEN 1 WHEN '02' THEN 1 WHEN '03' THEN 1 
											WHEN '04' THEN 2 WHEN '05' THEN 2 WHEN '06' THEN 2 
											WHEN '07' THEN 3 WHEN '08' THEN 3 WHEN '09' THEN 3 
											WHEN '10' THEN 4 WHEN '11' THEN 4 WHEN '12' THEN 4 
											END [FORECAST QUARTER],
		ROUND(CAST(REPLACE([Opening_Cash_TLF],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [OPENING CASH TLF EUR],
		ROUND(CAST(REPLACE([Opening_Cash_TLG],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [OPENING CASH TLG EUR],
		ROUND(CAST(REPLACE([Opening_Cash_Banks],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [OPENING CASH BANKS EUR],
		ROUND(CAST(REPLACE([Opening_Bank_deposits],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [OPENING BANK DEPOSITS EUR],
		ROUND(CAST(REPLACE([Opening_Borrowing_TLF],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [OPENING BORRROWING TLF EUR],
		ROUND(CAST(REPLACE([Opening_Borrowing_TLG],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [OPENING BORRROWING TLG EUR],
		ROUND(CAST(REPLACE([Opening_Borrowing_Banks],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [OPENING OPENING BORRROWING BANKS EUR],
		ROUND(CAST(REPLACE([Opening_NFP],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [OPENING NFP EUR],
        ROUND(CAST(REPLACE([Collections_inside_Netting],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [COLLECTIONS INSIDE NETTING EUR],
		ROUND(CAST(REPLACE([Collections_outside_Netting],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [COLLECTIONS OUTSIDE NETTING EUR],
		ROUND(CAST(REPLACE([Collections___External_trade_receivables],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [COLLECTIONS EXTERNAL TRADE RECEIVABLES EUR],
		ROUND(CAST(REPLACE([Sales_of_capital_equipment],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [SALES OF CAPITAL EQUIPMENT EUR],
		ROUND(CAST(REPLACE([External_divestments],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [EXTERNAL DIVESTMENTS EUR],
		ROUND(CAST(REPLACE([Income_taxes_refunds],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [INCOME TAXES REFUNDS EUR],
		ROUND(CAST(REPLACE([VAT_refunds],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [VAT REFUNDS EUR],
		ROUND(CAST(REPLACE([Other_taxes_and_duties_refunds],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [OTHER TAXES AND DUTIES REFUNDS EUR],
		ROUND(CAST(REPLACE([Dividend_collected___Capital_increase],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [DIVIDEND COLLECTED CAPITAL INCREASE EUR],
		ROUND(CAST(REPLACE([Royalties_collected],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [ROYALTIES COLLECTED EUR],
		ROUND(CAST(REPLACE([Financial_inflows___Internal],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [FINANCIAL INFLOWS INTERNAL EUR],
		ROUND(CAST(REPLACE([Financial_inflows___External],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [FINANCIAL INFLOWS EXTERNAL EUR],
		ROUND(CAST(REPLACE([Other_cash_inflows___Internal],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [OTHER CASH INFLOWS INTERNAL EUR],
		ROUND(CAST(REPLACE([Other_cash_inflows___External],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [OTHER CASH INFLOWS EXTERNAL EUR],
		ROUND(CAST(REPLACE([Total_Inflows],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [TOTAL INFLOWS EUR],
		ROUND(CAST(REPLACE([Payments_inside_Netting],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [PAYMENTS INSIDE NETTING EUR],
		ROUND(CAST(REPLACE([Payments_outside_Netting],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [PAYMENTS OUTSIDE NETTING EUR],
		ROUND(CAST(REPLACE([Payments___External_trade__payables],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [PAYMENTS EXTERNAL TRADE PAYBABLES],
		ROUND(CAST(REPLACE([_Discounts__rebates__market_support_paid_],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [DISCOUNTS REBATS MARKET SUPPORT PAID EUR],
		ROUND(CAST(REPLACE([External_Investments],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [EXTERNAL INVESTMENTS EUR],
		ROUND(CAST(REPLACE([Income_taxes_paid],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [INCOME TAXES PAID EUR],
		ROUND(CAST(REPLACE([VAT_paid],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [VAT PAID EUR],
		ROUND(CAST(REPLACE([Other_taxes_and_duties_paid],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [OTHER TAXES AND DUTIES PAID EUR],
		ROUND(CAST(REPLACE([Dividend_paid___capital_reduction],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [DIVIDEND PAID CAPITAL REDUCTION EUR],
		ROUND(CAST(REPLACE([Royalties_paid],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [ROYALTIES PAID EUR],
        ROUND(CAST(REPLACE([_Salaries__social_securities_and_pensions_],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [SALARIES SOCIAL SECURITIES AND PENSIONS EUR],
		ROUND(CAST(REPLACE([Financial_outflows___external],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [FINANCIAL OUTFLOWS INTERNAL EUR],
		ROUND(CAST(REPLACE([Financial_outflows___Internal],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [FINANCIAL OUTFLOWS EXTERNAL EUR],	
		ROUND(CAST(REPLACE([Other_cash_outflows___Internal],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [OTHER CASH OUTFLOWS INTERNAL EUR],
		ROUND(CAST(REPLACE([Other_cash_outflows___External],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [OTHER CASH OUTFLOWS EXTERNAL EUR],
		ROUND(CAST(REPLACE([Total_Ouflows],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [TOTAL OUTFLOWS EUR],
		ROUND(CAST(REPLACE([Net_cash_flow],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [NET CASH FLOW EUR],
		ROUND(CAST(REPLACE([Closing_Cash_TLF],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [CLOSING CASH TLF EUR],
		ROUND(CAST(REPLACE([Closing_Cash_TLG],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [CLOSING CASH TLG EUR],
		ROUND(CAST(REPLACE([Closing_Cash_Banks],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [CLOSING CASH BANKS EUR],
		ROUND(CAST(REPLACE([Closing_Bank_deposits],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [CLOSING BANK DEPOSITS EUR],
		ROUND(CAST(REPLACE([Closing_Borrowing_TLF],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [CLOSING BORROWING TLF EUR],
		ROUND(CAST(REPLACE([Closing_Borrowing_TLG],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [CLOSING BORROWING TLG EUR],
		ROUND(CAST(REPLACE([Closing_Borrowing_Banks],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [CLOSING BORROWING BANKS EUR],
		ROUND(CAST(REPLACE([Total_Net],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [TOTAL NET EUR],
		ROUND(CAST(REPLACE([Peak_financing],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [PEAK FINANCING EUR],
		ROUND(CAST(REPLACE([Positive_balance_of_cash_pool_members],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [POSITIVE BALANCE CASH POOL MEMBERS],
		ROUND(CAST(REPLACE([Total_Peak],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [TOTAL PEAK EUR],
		ROUND(CAST(REPLACE([Net_cash_flow],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [NET FLOWS EUR],
		ROUND(CAST(REPLACE([Total_Inflows],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) + ABS(ROUND(CAST(REPLACE([Total_Ouflows],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0)) AS [GROSS FLOWS EUR],
		ROUND(CAST(REPLACE([Net_cash_flow],',','') as bigint) + CAST(REPLACE([Dividend_paid___capital_reduction],',','') as bigint) - CAST(REPLACE([Dividend_collected___Capital_increase] ,',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [NET W/O DIVIDEND FLOWS EUR],
        ROUND(CAST(REPLACE([Total_Inflows],',','') as bigint) - CAST(REPLACE([Dividend_collected___Capital_increase],',','') as bigint)/CAST([FX_Rates] AS FLOAT) + ABS(CAST(REPLACE([Total_Ouflows],',','') as bigint) + CAST(REPLACE([Dividend_paid___capital_reduction],',','') as bigint))/CAST([FX_Rates] AS FLOAT),0) AS [GROSS W/O DIVIDEND FLOWS EUR]
        
FROM [Tetra_AnalyticsDB].[dbo].[CF_2019_10] CF_PERIOD
UNION ALL
SELECT 

		CF_PERIOD.[FAS_Code] AS [FAS CODE],
        SUBSTRING(Package_period,4,4) + ' ' + SUBSTRING(Package_period,9,2) AS [FORECAST VERSION],
		SUBSTRING(Package_period,4,4) AS [FORECAST VERSION YEAR],
		SUBSTRING(Package_period,9,2) AS [FORECAST VERSION PERIOD],
		CASE SUBSTRING(Package_period,9,2) WHEN '01' THEN 1 WHEN '02' THEN 1 WHEN '03' THEN 1 
											WHEN '04' THEN 2 WHEN '05' THEN 2 WHEN '06' THEN 2 
											WHEN '07' THEN 3 WHEN '08' THEN 3 WHEN '09' THEN 3 
											WHEN '10' THEN 4 WHEN '11' THEN 4 WHEN '12' THEN 4 
											END [FORECAST VERSION QUARTER],
		CASE WHEN SUBSTRING(Package_period,4,4) = SUBSTRING(Forecast_period,4,4) AND SUBSTRING(Forecast_period,9,2) < SUBSTRING(Package_period,9,2) THEN 'ACTUAL' ELSE 'FORECAST' END [FORECAST TYPE],
		CASE WHEN SUBSTRING(Package_period,4,4) = SUBSTRING(Forecast_period,4,4) AND SUBSTRING(Forecast_period,9,2) < SUBSTRING(Package_period,9,2) THEN 1 ELSE 2 END [FORECAST TYPE NUM],
        SUBSTRING(Forecast_period,4,4)  AS [FORECAST YEAR],
		SUBSTRING(Forecast_period,9,2)  AS [FORECAST PERIOD],
		CASE SUBSTRING(Forecast_period,9,2) WHEN '01' THEN 1 WHEN '02' THEN 1 WHEN '03' THEN 1 
											WHEN '04' THEN 2 WHEN '05' THEN 2 WHEN '06' THEN 2 
											WHEN '07' THEN 3 WHEN '08' THEN 3 WHEN '09' THEN 3 
											WHEN '10' THEN 4 WHEN '11' THEN 4 WHEN '12' THEN 4 
											END [FORECAST QUARTER],
		ROUND(CAST(REPLACE([Opening_Cash_TLF],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [OPENING CASH TLF EUR],
		ROUND(CAST(REPLACE([Opening_Cash_TLG],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [OPENING CASH TLG EUR],
		ROUND(CAST(REPLACE([Opening_Cash_Banks],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [OPENING CASH BANKS EUR],
		ROUND(CAST(REPLACE([Opening_Bank_deposits],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [OPENING BANK DEPOSITS EUR],
		ROUND(CAST(REPLACE([Opening_Borrowing_TLF],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [OPENING BORRROWING TLF EUR],
		ROUND(CAST(REPLACE([Opening_Borrowing_TLG],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [OPENING BORRROWING TLG EUR],
		ROUND(CAST(REPLACE([Opening_Borrowing_Banks],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [OPENING OPENING BORRROWING BANKS EUR],
		ROUND(CAST(REPLACE([Opening_NFP],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [OPENING NFP EUR],
        ROUND(CAST(REPLACE([Collections_inside_Netting],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [COLLECTIONS INSIDE NETTING EUR],
		ROUND(CAST(REPLACE([Collections_outside_Netting],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [COLLECTIONS OUTSIDE NETTING EUR],
		ROUND(CAST(REPLACE([Collections___External_trade_receivables],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [COLLECTIONS EXTERNAL TRADE RECEIVABLES EUR],
		ROUND(CAST(REPLACE([Sales_of_capital_equipment],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [SALES OF CAPITAL EQUIPMENT EUR],
		ROUND(CAST(REPLACE([External_divestments],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [EXTERNAL DIVESTMENTS EUR],
		ROUND(CAST(REPLACE([Income_taxes_refunds],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [INCOME TAXES REFUNDS EUR],
		ROUND(CAST(REPLACE([VAT_refunds],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [VAT REFUNDS EUR],
		ROUND(CAST(REPLACE([Other_taxes_and_duties_refunds],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [OTHER TAXES AND DUTIES REFUNDS EUR],
		ROUND(CAST(REPLACE([Dividend_collected___Capital_increase],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [DIVIDEND COLLECTED CAPITAL INCREASE EUR],
		ROUND(CAST(REPLACE([Royalties_collected],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [ROYALTIES COLLECTED EUR],
		ROUND(CAST(REPLACE([Financial_inflows___Internal],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [FINANCIAL INFLOWS INTERNAL EUR],
		ROUND(CAST(REPLACE([Financial_inflows___External],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [FINANCIAL INFLOWS EXTERNAL EUR],
		ROUND(CAST(REPLACE([Other_cash_inflows___Internal],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [OTHER CASH INFLOWS INTERNAL EUR],
		ROUND(CAST(REPLACE([Other_cash_inflows___External],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [OTHER CASH INFLOWS EXTERNAL EUR],
		ROUND(CAST(REPLACE([Total_Inflows],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [TOTAL INFLOWS EUR],
		ROUND(CAST(REPLACE([Payments_inside_Netting],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [PAYMENTS INSIDE NETTING EUR],
		ROUND(CAST(REPLACE([Payments_outside_Netting],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [PAYMENTS OUTSIDE NETTING EUR],
		ROUND(CAST(REPLACE([Payments___External_trade__payables],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [PAYMENTS EXTERNAL TRADE PAYBABLES],
		ROUND(CAST(REPLACE([_Discounts__rebates__market_support_paid_],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [DISCOUNTS REBATS MARKET SUPPORT PAID EUR],
		ROUND(CAST(REPLACE([External_Investments],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [EXTERNAL INVESTMENTS EUR],
		ROUND(CAST(REPLACE([Income_taxes_paid],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [INCOME TAXES PAID EUR],
		ROUND(CAST(REPLACE([VAT_paid],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [VAT PAID EUR],
		ROUND(CAST(REPLACE([Other_taxes_and_duties_paid],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [OTHER TAXES AND DUTIES PAID EUR],
		ROUND(CAST(REPLACE([Dividend_paid___capital_reduction],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [DIVIDEND PAID CAPITAL REDUCTION EUR],
		ROUND(CAST(REPLACE([Royalties_paid],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [ROYALTIES PAID EUR],
        ROUND(CAST(REPLACE([_Salaries__social_securities_and_pensions_],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [SALARIES SOCIAL SECURITIES AND PENSIONS EUR],
		ROUND(CAST(REPLACE([Financial_outflows___external],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [FINANCIAL OUTFLOWS INTERNAL EUR],
		ROUND(CAST(REPLACE([Financial_outflows___Internal],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [FINANCIAL OUTFLOWS EXTERNAL EUR],	
		ROUND(CAST(REPLACE([Other_cash_outflows___Internal],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [OTHER CASH OUTFLOWS INTERNAL EUR],
		ROUND(CAST(REPLACE([Other_cash_outflows___External],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [OTHER CASH OUTFLOWS EXTERNAL EUR],
		ROUND(CAST(REPLACE([Total_Ouflows],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [TOTAL OUTFLOWS EUR],
		ROUND(CAST(REPLACE([Net_cash_flow],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [NET CASH FLOW EUR],
		ROUND(CAST(REPLACE([Closing_Cash_TLF],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [CLOSING CASH TLF EUR],
		ROUND(CAST(REPLACE([Closing_Cash_TLG],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [CLOSING CASH TLG EUR],
		ROUND(CAST(REPLACE([Closing_Cash_Banks],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [CLOSING CASH BANKS EUR],
		ROUND(CAST(REPLACE([Closing_Bank_deposits],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [CLOSING BANK DEPOSITS EUR],
		ROUND(CAST(REPLACE([Closing_Borrowing_TLF],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [CLOSING BORROWING TLF EUR],
		ROUND(CAST(REPLACE([Closing_Borrowing_TLG],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [CLOSING BORROWING TLG EUR],
		ROUND(CAST(REPLACE([Closing_Borrowing_Banks],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [CLOSING BORROWING BANKS EUR],
		ROUND(CAST(REPLACE([Total_Net],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [TOTAL NET EUR],
		ROUND(CAST(REPLACE([Peak_financing],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [PEAK FINANCING EUR],
		ROUND(CAST(REPLACE([Positive_balance_of_cash_pool_members],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [POSITIVE BALANCE CASH POOL MEMBERS],
		ROUND(CAST(REPLACE([Total_Peak],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [TOTAL PEAK EUR],
		ROUND(CAST(REPLACE([Net_cash_flow],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [NET FLOWS EUR],
	    ROUND(CAST(REPLACE([Total_Inflows],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) + ABS(ROUND(CAST(REPLACE([Total_Ouflows],',','') as bigint)/CAST([FX_Rates] AS FLOAT),0)) AS [GROSS FLOWS EUR],
		ROUND(CAST(REPLACE([Net_cash_flow],',','') as bigint) + CAST(REPLACE([Dividend_paid___capital_reduction],',','') as bigint) - CAST(REPLACE([Dividend_collected___Capital_increase] ,',','') as bigint)/CAST([FX_Rates] AS FLOAT),0) AS [NET W/O DIVIDEND FLOWS EUR],
        ROUND(CAST(REPLACE([Total_Inflows],',','') as bigint) - CAST(REPLACE([Dividend_collected___Capital_increase],',','') as bigint)/CAST([FX_Rates] AS FLOAT) + ABS(CAST(REPLACE([Total_Ouflows],',','') as bigint) + CAST(REPLACE([Dividend_paid___capital_reduction],',','') as bigint))/CAST([FX_Rates] AS FLOAT),0) AS [GROSS W/O DIVIDEND FLOWS EUR]
        
FROM [Tetra_AnalyticsDB].[dbo].[CF_2019_11] CF_PERIOD	
		) as CF_JOINED

ALTER TABLE CASHFLOW_FORECAST_MAIN ALTER COLUMN [FAS CODE] NVARCHAR(50) NOT NULL;
ALTER TABLE CASHFLOW_FORECAST_MAIN ALTER COLUMN [FORECAST VERSION] NVARCHAR(50) NOT NULL;
ALTER TABLE CASHFLOW_FORECAST_MAIN ALTER COLUMN [FORECAST YEAR] NVARCHAR(50) NOT NULL;
ALTER TABLE CASHFLOW_FORECAST_MAIN ALTER COLUMN [FORECAST PERIOD] NVARCHAR(50) NOT NULL;
ALTER TABLE CASHFLOW_FORECAST_MAIN ALTER COLUMN [FORECAST TYPE] NVARCHAR(50) NOT NULL;
ALTER TABLE CASHFLOW_FORECAST_MAIN ADD CONSTRAINT PK_CASHFLOW_FORECAST PRIMARY KEY ([FAS CODE],[FORECAST VERSION],[FORECAST YEAR],[FORECAST PERIOD],[FORECAST TYPE])

SELECT [FAS CODE],[FORECAST PERIOD],[FORECAST QUARTER],[FORECAST TYPE],[FORECAST TYPE NUM],[FORECAST VERSION],[FORECAST VERSION PERIOD],[FORECAST VERSION QUARTER],[FORECAST VERSION YEAR],[FORECAST YEAR],[CATEGORY],[AMOUNT]
INTO CASHFLOW_FORECAST_MAIN_UNPIVOT
FROM CASHFLOW_FORECAST_MAIN

UNPIVOT
(
	AMOUNT
	FOR CATEGORY IN ([OPENING CASH TLF EUR],
[OPENING CASH TLG EUR],
[OPENING CASH BANKS EUR],
[OPENING BANK DEPOSITS EUR],
[OPENING BORRROWING TLF EUR],
[OPENING BORRROWING TLG EUR],
[OPENING OPENING BORRROWING BANKS EUR],
--[OPENING NFP EUR],
[COLLECTIONS INSIDE NETTING EUR],
[COLLECTIONS OUTSIDE NETTING EUR],
[COLLECTIONS EXTERNAL TRADE RECEIVABLES EUR],
[SALES OF CAPITAL EQUIPMENT EUR],
[EXTERNAL DIVESTMENTS EUR],
[INCOME TAXES REFUNDS EUR],
[VAT REFUNDS EUR],
[OTHER TAXES AND DUTIES REFUNDS EUR],
[DIVIDEND COLLECTED CAPITAL INCREASE EUR],
[ROYALTIES COLLECTED EUR],
[FINANCIAL INFLOWS INTERNAL EUR],
[FINANCIAL INFLOWS EXTERNAL EUR],
[OTHER CASH INFLOWS INTERNAL EUR],
[OTHER CASH INFLOWS EXTERNAL EUR],
--[TOTAL INFLOWS EUR],
[PAYMENTS INSIDE NETTING EUR],
[PAYMENTS OUTSIDE NETTING EUR],
[PAYMENTS EXTERNAL TRADE PAYBABLES],
[DISCOUNTS REBATS MARKET SUPPORT PAID EUR],
[EXTERNAL INVESTMENTS EUR],
[INCOME TAXES PAID EUR],
[VAT PAID EUR],
[OTHER TAXES AND DUTIES PAID EUR],
[DIVIDEND PAID CAPITAL REDUCTION EUR],
[ROYALTIES PAID EUR],
[SALARIES SOCIAL SECURITIES AND PENSIONS EUR],
[FINANCIAL OUTFLOWS INTERNAL EUR],
[FINANCIAL OUTFLOWS EXTERNAL EUR],
[OTHER CASH OUTFLOWS INTERNAL EUR],
[OTHER CASH OUTFLOWS EXTERNAL EUR],
--[TOTAL OUTFLOWS EUR],
--[NET CASH FLOW EUR],
[CLOSING CASH TLF EUR],
[CLOSING CASH TLG EUR],
[CLOSING CASH BANKS EUR],
[CLOSING BANK DEPOSITS EUR],
[CLOSING BORROWING TLF EUR],
[CLOSING BORROWING TLG EUR],
[CLOSING BORROWING BANKS EUR],
--[TOTAL NET EUR],
[PEAK FINANCING EUR],
[POSITIVE BALANCE CASH POOL MEMBERS],
--[TOTAL PEAK EUR],
[NET FLOWS EUR],
[GROSS FLOWS EUR],
[NET W/O DIVIDEND FLOWS EUR],
[GROSS W/O DIVIDEND FLOWS EUR]) 
)AS CASHFLOW_FORECAST_MAIN_UNPIVOT


WITH 
CTE_ACTUAL AS
(
			SELECT 
            [FAS CODE],[FORECAST PERIOD],[FORECAST QUARTER], [FORECAST TYPE],[FORECAST TYPE NUM], [FORECAST VERSION],[FORECAST VERSION PERIOD],[FORECAST VERSION QUARTER],[FORECAST VERSION YEAR],[FORECAST YEAR],
			[CATEGORY],[AMOUNT],				
					ROW_NUMBER() OVER (PARTITION BY [FAS CODE],[FORECAST YEAR],[FORECAST PERIOD],[CATEGORY]
					ORDER BY [FAS CODE],[FORECAST VERSION],[FORECAST YEAR],[FORECAST PERIOD]) as IDX
			FROM   CASHFLOW_FORECAST_MAIN_UNPIVOT
			WHERE [FORECAST TYPE] = 'ACTUAL'
		
),

CTE_FORECAST AS
(

			SELECT  
			[FAS CODE],[FORECAST PERIOD],[FORECAST QUARTER],[FORECAST TYPE],[FORECAST TYPE NUM],[FORECAST VERSION],[FORECAST VERSION PERIOD],[FORECAST VERSION QUARTER],[FORECAST VERSION YEAR],[FORECAST YEAR],
		    [CATEGORY],[AMOUNT],			
					ROW_NUMBER() OVER (PARTITION BY [FAS CODE],[FORECAST YEAR],[FORECAST PERIOD],[CATEGORY]
					ORDER BY [FAS CODE],[FORECAST VERSION],[FORECAST YEAR],[FORECAST PERIOD]) as IDX
			FROM   CASHFLOW_FORECAST_MAIN_UNPIVOT
			WHERE [FORECAST TYPE] = 'FORECAST'
		
) 

SELECT
     
     CF_WITH_DIFF.*
	 
INTO CASHFLOW_FORECAST_MAIN_DIFF
FROM
(
SELECT
	CTE_FORECAST.[FAS CODE],
	CTE_FORECAST.[FORECAST PERIOD],
	CTE_FORECAST.[FORECAST QUARTER],
	CTE_FORECAST.[FORECAST TYPE],
	CTE_FORECAST.[FORECAST TYPE NUM],
	CTE_FORECAST.[FORECAST VERSION],
	CTE_FORECAST.[FORECAST VERSION PERIOD],
	CTE_FORECAST.[FORECAST VERSION QUARTER],
	CTE_FORECAST.[FORECAST VERSION YEAR],
	CTE_FORECAST.[FORECAST YEAR],
	CTE_FORECAST.[CATEGORY],
	CTE_FORECAST.[AMOUNT],
	CTE_FORECAST.AMOUNT - CTE_ACTUAL.AMOUNT AS AMOUNT_DIFF,
	COALESCE(NULLIF(CTE_ACTUAL.AMOUNT - CTE_FORECAST.AMOUNT,0)/NULLIF(CTE_ACTUAL.AMOUNT,0),0) AS PERC_DIFF
FROM CTE_ACTUAL 

	FULL OUTER JOIN CTE_FORECAST ON 
		 CTE_ACTUAL.[FAS CODE] = CTE_FORECAST.[FAS CODE] 
	AND  CTE_ACTUAL.[FORECAST YEAR] = CTE_FORECAST.[FORECAST YEAR] 
	AND  CTE_ACTUAL.[FORECAST PERIOD] = CTE_FORECAST.[FORECAST PERIOD] 
    AND  CTE_ACTUAL.[CATEGORY] = CTE_FORECAST.[CATEGORY]
UNION
SELECT 
	CTE_ACTUAL.[FAS CODE],
	CTE_ACTUAL.[FORECAST PERIOD],
	CTE_ACTUAL.[FORECAST QUARTER],
	CTE_ACTUAL.[FORECAST TYPE],
	CTE_ACTUAL.[FORECAST TYPE NUM],
	CTE_ACTUAL.[FORECAST VERSION],
	CTE_ACTUAL.[FORECAST VERSION PERIOD],
	CTE_ACTUAL.[FORECAST VERSION QUARTER],
	CTE_ACTUAL.[FORECAST VERSION YEAR],
	CTE_ACTUAL.[FORECAST YEAR],
	CTE_ACTUAL.[CATEGORY],
	CTE_ACTUAL.[AMOUNT],
	0 AS AMOUNT_DIFF,
    0 AS PERC_DIFF
FROM CTE_ACTUAL ) AS CF_WITH_DIFF



/*
select * from CASHFLOW_FORECAST_MAIN_DIFF WHERE [FAS CODE] = '1060003' AND [FORECAST YEAR] = '2019'
AND [FORECAST PERIOD] = '01' AND CATEGORY = 'CLOSING BANK DEPOSITS EUR'

select * from CASHFLOW_FORECAST_MAIN_DIFF DIFF
JOIN CF_IG_Entities CF on DIFF.[FAS CODE] = Cf.[FAS CODE] 
WHERE [COUNTRY] = 'Australia' AND [FORECAST YEAR] = '2019' --and [FORECAST VERSION] = '2018 11'
AND [FORECAST PERIOD] = '01' AND CATEGORY = 'TOTAL NET EUR' and IG = 'TP'
  **/


/*
;WITH CTE AS
(
			SELECT 
					[FORECAST VERSION],[FORECAST TYPE],[FAS CODE],[FORECAST YEAR],[FORECAST PERIOD],[CATEGORY],[AMOUNT],
					ROW_NUMBER() OVER (PARTITION BY [FAS CODE],[FORECAST YEAR],[FORECAST PERIOD],[CATEGORY]
					ORDER BY [FAS CODE],[FORECAST VERSION],[FORECAST YEAR],[FORECAST PERIOD]) as IDX
			FROM   CASHFLOW_FORECAST_MAIN_UNPIVOT
			WHERE  [FAS CODE] = '0010001'
		
) 

select [FAS CODE],
	   [FORECAST PERIOD],
	   [FORECAST QUARTER],
	   [FORECAST TYPE],
	   [FORECAST TYPE NUM],
	   [FORECAST VERSION],
	   [FORECAST VERSION PERIOD],
	   [FORECAST YEAR],
	   CASE WHEN [FORECAST TYPE] = 'ACTUAL' THEN 0 ELSE [FORECAST VARIANCE AMOUNT] END,
	   CASE WHEN [FORECAST TYPE] = 'ACTUAL' THEN 0 ELSE [FORECAST VARIANCE %] END,
	   [CATEGORY]
FROM CASHFLOW_FORECAST_MAIN_UNPIVOT CFM
JOIN CTE C1 ON CFM.[FAS CODE] = C1.[FAS CODE] AND CFM.CATEGORY = C1.[CATEGORY] = AND CFM.[FORECAST YEAR] = C1.[FORECAST YEAR] AND CFM.[FORECAST PERIOD] = C1.[FORECAST PERIOD]
JOIN CTE C2 ON C1.[FAS CODE] = C1.[FAS CODE] AND C1.CATEGORY = C1.[CATEGORY] = AND C1.[FORECAST YEAR] = C1.[FORECAST YEAR] AND C1.[FORECAST PERIOD] = C1.[FORECAST PERIOD]

--select CONCAT('[',COLUMN_NAME,']',',') from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'CASHFLOW_FORECAST_MAIN' AND COLUMN_NAME LIKE 'FO%' 
--select CONCAT('[',COLUMN_NAME,']',',') from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'CASHFLOW_FORECAST_MAIN' AND COLUMN_NAME NOT LIKE 'FO%'

**/



--select * from CASHFLOW_FORECAST_MAIN where [FORECAST VERSION] = '2019 09' AND [FAS CODE] = '0010001'


select CFM.[FAS CODE],entity.CLUSTER,entity.IG,entity.COUNTRY from CASHFLOW_FORECAST_MAIN_DIFF CFM
JOIN CF_IG_Entities entity on CFM.[FAS CODE] = entity.[FAS CODE] where CLUSTER = 'E&CA'

select * from CF_IG_Entities where IG = 'TP' 


select * from CASHFLOW_FORECAST_MAIN_DIFF

ALTER TABLE CF_2019_11
ALTER COLUMN FAS_Code NVARCHAR(50);