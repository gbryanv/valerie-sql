USE [Tetra_Audit_Analytics_PRD]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

DROP PROCEDURE IF EXISTS sp_CREDITRISK_updateCreditRisk
GO

CREATE PROCEDURE sp_CREDITRISK_updateCreditRisk
with exec as caller
as
begin

exec [Tetra_Audit_Analytics_PRD].dbo.sp_CREDITRISK_updateCreditLimits
exec [Tetra_Audit_Analytics_PRD].dbo.sp_CREDITRISK_updateCreditLimitChanges
exec [Tetra_Audit_Analytics_PRD].dbo.sp_CREDITRISK_updateAnnualPurchases
--exec [Tetra_Audit_Analytics_PRD].dbo.sp_CREDITRISK_updateReceivables

END

GO