
USE [Tetra_Audit_Analytics_PRD]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

DROP PROCEDURE IF EXISTS sp_CREDITRISK_updateCustomerReceivables
GO

CREATE PROCEDURE sp_CREDITRISK_updateCustomerReceivables
with exec as caller
as
begin

DROP TABLE IF EXISTS #GrossReceivablesStep1
select BKPF_BUKRS, BKPF_GJAHR, BKPF_MONAT, BSEG_AUGDT, BKPF_BELNR, BSEG_KUNNR, BKPF_WAERS, BSEG_KKBER as KKBER,
        SUM(BSEG_WRBTR*CASE WHEN BSEG_SHKZG = 'S' THEN 1 ELSE -1 END*[dbo].[ufnCurrencyTransform_TCURX](BKPF_WAERS)) as AmountTotal
into #GrossReceivablesStep1
from tbl_CREDITRISK_billedItemsBreakdown
group by BKPF_BUKRS, BKPF_GJAHR, BKPF_MONAT, BSEG_AUGDT, BKPF_BELNR, BSEG_KUNNR, BKPF_WAERS, BSEG_KKBER

DROP TABLE IF EXISTS tbl_CREDITRISK_creditSegmentReceivablesBreakdown
Select CONCAT(KKBER, BSEG_KUNNR) as PrimaryKey, BSEG_KUNNR as SiteID, KKBER as SegmentID, BKPF_BUKRS as CompanyCode, BKPF_GJAHR as FiscalYear, BSEG_AUGDT as ClearingDate, BKPF_BELNR as DocumentID, CxFA.ConvertedVal as Amount
into tbl_CREDITRISK_creditSegmentReceivablesBreakdown
from #GrossReceivablesStep1
OUTER APPLY (SELECT Val AS ConvertedVal, FinalCurr, Trans
    FROM dbo.ufnCustomCurrencyBudgetRate(BKPF_WAERS, 'EUR', '2018', AmountTotal)) CxFA(ConvertedVal,FinalCurr,Trans)


DROP TABLE IF EXISTS tbl_CREDITRISK_creditSegmentReceivablesAggregated
select aa.SiteID, aa.SiteName, aa.CreditSegment,
        ISNULL(aa.AmountEUR,0) + ISNULL(bb.BalanceCarryForwardEUR,0) as GrossReceivablesEUR
into tbl_CREDITRISK_creditSegmentReceivablesAggregated
from (
        Select SiteID, SiteName, CreditSegment, sum(AmountEUR) as AmountEUR
        from tbl_CREDITRISK_creditSegmentReceivablesBreakdown
        group by SiteID, SiteName, CreditSegment
    ) aa
left join (
        Select SiteID, SiteName, CreditSegment, sum(BalanceCarryForwardEUR) as BalanceCarryForwardEUR
        from tbl_CREDITRISK_creditSegmentReceivablesBalanceCarryForward
        group by SiteID, SiteName, CreditSegment
    ) bb
on aa.SiteID = bb.SiteID
and aa.SiteName = bb.SiteName
and aa.CreditSegment = bb.CreditSegment
group by aa.SiteID, aa.SiteName, aa.CreditSegment, aa.AmountEUR, bb.BalanceCarryForwardEUR



-- ################################################################
-- ################ Daily Gross Receivables Tables ################
-- ################################################################


drop table if exists #tmpDailyStep1
select SiteID, SegmentID, BalanceCarryForward
into #tmpDailyStep1
from tbl_CREDITRISK_creditSegmentReceivablesBalanceCarryForward

drop table if exists #tmpDailyStep2
SELECT DISTINCT SiteID, SegmentID, ClearingDate, sum(Amount) as AmountTotal
into #tmpDailyStep2
from tbl_CREDITRISK_creditSegmentReceivablesBreakdown
where ClearingDate <> '19000101'
group by SiteID, SegmentID, ClearingDate


drop table if exists #tmpDailyStep3
select aa.*, bb.date_id as DayDate, ISNULL(cc.AmountTotal,0) as AmountTotal
into #tmpDailyStep3
from #tmpDailyStep1 aa
left join tbl_CREDITRISK_CalendarDaily2019 bb
on 1=1
left join #tmpDailyStep2 cc
on cc.SiteID = aa.SiteID
and cc.SegmentID = aa.SegmentID
and cc.ClearingDate = bb.date_id


drop table if exists #tmpDailyStep4
SELECT SiteID, SegmentID, DayDate, BalanceCarryForward, AmountTotal, SUM(AmountTotal)
OVER (PARTITION BY SiteID, SegmentID ORDER BY DayDate)
AS CurrentBalance
Into #tmpDailyStep4
From #tmpDailyStep3

drop table if exists #tmpDailyStep5
select SiteID, SegmentID, DayDate, (BalanceCarryForward + CurrentBalance) as GrossReceivablesEUR
into #tmpDailyStep5
from #tmpDailyStep4

drop table if exists tbl_CREDITRISK_creditSegmentReceivablesDaily
select *
into tbl_CREDITRISK_creditSegmentReceivablesDaily
from #tmpDailyStep5


drop table if exists tbl_CREDITRISK_creditSegmentReceivablesDailyName
select aa.*, KNA1.NAME1
into tbl_CREDITRISK_creditSegmentReceivablesDailyName
from tbl_CREDITRISK_creditSegmentReceivablesDaily aa 
inner join KNA1
on aa.SiteID = KNA1.KUNNR
and KNA1.MANDT = '100'

END

GO

