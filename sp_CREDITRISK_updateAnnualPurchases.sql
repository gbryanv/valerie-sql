USE [Tetra_Audit_Analytics_PRD]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE sp_CREDITRISK_updateAnnualPurchases 
with exec as caller
as
begin

DROP TABLE IF EXISTS tbl_CREDITRISK_billedItemsBreakdown

SELECT distinct
         BKPF.BELNR as BKPF_BELNR
        ,BKPF.GJAHR as BKPF_GJAHR
        ,BKPF.BUKRS as BKPF_BUKRS
        ,BKPF.BLART as BKPF_BLART
        ,BKPF.WAERS as BKPF_WAERS
        ,BKPF.MONAT as BKPF_MONAT
        ,BKPF.BLDAT as BKPF_BLDAT
        ,BKPF.BUDAT as BKPF_BUDAT
        
        ,BSEG.AUGDT as BSEG_AUGDT
        ,BSEG.SHKZG as BSEG_SHKZG
        ,CAST(BSEG.WRBTR AS MONEY) as BSEG_WRBTR
        ,BSEG.KUNNR as BSEG_KUNNR
        ,BSEG.KKBER as BSEG_KKBER
		,BSEG.BSCHL as BSEG_BSCHL
        
        ,KNA1.NAME1 as KNA1_NAME1

        ,DATEDIFF(DAY, BKPF.BLDAT, BSEG.AUGDT) as PaymentsDays
        
into tbl_CREDITRISK_billedItemsBreakdown

FROM __Receivables_BKPF BKPF

INNER JOIN __Receivables_BSEG BSEG
ON BSEG.MANDT = BKPF.MANDT
AND BSEG.BELNR = BKPF.BELNR
AND BSEG.BUKRS = BKPF.BUKRS
AND BSEG.GJAHR = BKPF.GJAHR

INNER JOIN KNA1
  ON KNA1.KUNNR = BSEG.KUNNR
  AND KNA1.MANDT = BSEG.MANDT

INNER JOIN T001CM 
  ON BKPF.BUKRS = T001CM.BUKRS

WHERE BKPF.GJAHR = '2019'
  AND KNA1.KTOKD <> 'ICC'
  AND BSEG.KOART = 'D'
  AND BSEG.KKBER <> ''
  AND BSEG.KKBER IN (Select KKBER from T014T Where KKBER like '%1' or KKBER like '%3')


    
  -- #################################################################
  -- ####################### ANNUAL PURCHASES ########################
  -- #################################################################


DROP TABLE IF EXISTS #tmp3

SELECT BKPF_BUKRS, BKPF_GJAHR,BKPF_BELNR, BKPF_MONAT,BSEG_BSCHL, BSEG_SHKZG, BSEG_KKBER, BSEG_WRBTR, BKPF_WAERS, BSEG_KUNNR, KNA1_NAME1, BSEG_AUGDT, BKPF_BLART
INTO #tmp3
FROM tbl_CREDITRISK_billedItemsBreakdown

WHERE BKPF_BLART = 'RV'
and BSEG_AUGDT is not NULL


DROP TABLE IF EXISTS #tmpPurchasesStep1
select BKPF_BUKRS, BKPF_GJAHR, BKPF_BELNR, BKPF_MONAT, BSEG_SHKZG, BKPF_WAERS, BSEG_AUGDT, BSEG_KUNNR, KNA1_NAME1, BSEG_KKBER, SUM(BSEG_WRBTR*[dbo].[ufnCurrencyTransform_TCURX](BKPF_WAERS)) as AmountTotal
into #tmpPurchasesStep1
from #tmp3
group by BKPF_BUKRS, BKPF_GJAHR, BKPF_BELNR, BKPF_MONAT, BSEG_SHKZG, BKPF_WAERS, BSEG_AUGDT, BSEG_KUNNR, KNA1_NAME1, BSEG_KKBER

DROP TABLE IF EXISTS tbl_CREDITRISK_annualPurchasesBreakdown
select CONCAT(BSEG_KKBER, BSEG_KUNNR) as PrimaryKey, BSEG_KUNNR as SiteID, BSEG_KKBER as SegmentID, BKPF_GJAHR as FiscalYear, CxFA.ConvertedVal as Amount, BKPF_BUKRS as CompanyCodeID, BSEG_AUGDT as TransactionDate, BKPF_BELNR as DocumentID
into tbl_CREDITRISK_annualPurchasesBreakdown
from #tmpPurchasesStep1
OUTER APPLY (SELECT Val AS ConvertedVal, FinalCurr, Trans
    FROM dbo.ufnCustomCurrencyBudgetRate(BKPF_WAERS, 'EUR', '2018', AmountTotal*CASE WHEN BSEG_SHKZG = 'S' THEN 1 ELSE -1 END)) CxFA(ConvertedVal,FinalCurr,Trans)

DROP TABLE IF EXISTS #tmpAnnualPurchasesTurnover
SELECT distinct CompanyCodeID, sum(Amount) as AnnualTurnover
into #tmpAnnualPurchasesTurnover
from tbl_CREDITRISK_annualPurchasesBreakdown
group by CompanyCodeID

DROP TABLE IF EXISTS tbl_CREDITRISK_annualPurchasesAggregated
SELECT DISTINCT aa.PrimaryKey, aa.CompanyCodeID, FiscalYear, SiteID, SegmentID, SUM(Amount) as AnnualPurchasesEUR,
        SUM(Amount)/CASE WHEN bb.AnnualTurnover = 0 THEN 1 ELSE bb.AnnualTurnover END as PurchasesTurnoverPercentage
into tbl_CREDITRISK_annualPurchasesAggregated
from tbl_CREDITRISK_annualPurchasesBreakdown aa
INNER JOIN #tmpAnnualPurchasesTurnover bb
on aa.CompanyCodeID = bb.CompanyCodeID
GROUP BY aa.PrimaryKey, aa.CompanyCodeID, FiscalYear, SiteID, SegmentID, bb.AnnualTurnover

DROP TABLE IF EXISTS tbl_CREDITRISK_annualPurchases
SELECT aa.*, t001.WAERS as Currency,
        CASE WHEN bb.SiteCheckException = '0' THEN bb.SiteCreditLimit ELSE
            CASE WHEN bb.CustomerCheckException = '0' THEN bb.CustomerCreditLimit ELSE
                bb.CustomerGroupCreditLimit
            END
        END as CreditLimit,
        CASE WHEN bb.SiteCheckException = '0' THEN bb.SiteCreditLimit ELSE
            CASE WHEN bb.CustomerCheckException = '0' THEN bb.CustomerCreditLimit ELSE
                bb.CustomerGroupCreditLimit
            END
        END / CASE WHEN cc.AnnualTurnover = 0 THEN 1 ELSE cc.AnnualTurnover END as LimitTurnoverPercentage,
        bb.CUSTOMER as CustomerID,
        bb.CustomerName as CustomerName
  INTO tbl_CREDITRISK_annualPurchases
  FROM tbl_CREDITRISK_annualPurchasesAggregated aa
  
  left join tbl_CREDITRISK_CreditLimits bb
  on bb.[SITEID] = aa.SiteID
  and bb.SiteCreditSgmnt = aa.SegmentID

  INNER JOIN #tmpAnnualPurchasesTurnover cc
  on aa.CompanyCode = cc.CompanyCode
  
  INNER JOIN T001 t001
  on t001.BUKRS = aa.CompanyCode

  END
  GO